import { enableProdMode } from '@angular/core';
import { platformBrowserDynamic } from '@angular/platform-browser-dynamic';

import { AppModule } from './app/app.module';
import { environment } from './environments/environment';
import Amplify, { I18n, Cache } from 'aws-amplify';
import awsConfigure from 'src/aws-exports';
import { Lang } from '@cedu/i18n';
import { StorageHelper } from '@cedu-helpers/storage.helper';
import 'hammerjs';

const storageHelper = new StorageHelper();
const currentLang = storageHelper.getItem('Lang');
const lang = currentLang ? currentLang : 'en';

Amplify.configure(awsConfigure);
I18n.putVocabularies(Lang);
I18n.setLanguage(lang);
Cache.configure({
  itemMaxSize: 200000, // 200KB
  defaultPriority: 4,
  storage: window.localStorage,
  defaultTTL: 1, // 1h
});
if (environment.production) {
  enableProdMode();
}

platformBrowserDynamic().bootstrapModule(AppModule)
  .catch(err => err);
