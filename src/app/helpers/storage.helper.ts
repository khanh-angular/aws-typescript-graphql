export class StorageHelper {
    getItem(item: string) {
        if (item) {
            const localItem = localStorage.getItem(item);
            if (localItem) {
                return JSON.parse(localItem);
            }
            return null;
        }
    }
    setItem(item: string, data: any) {
        return localStorage.setItem(item, JSON.stringify(data));
    }
}
