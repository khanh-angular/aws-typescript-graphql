import * as moment from 'moment-timezone';
import { Timezone } from '@cedu-constants/timezone.const';

export function getTimezone(datetime?: any) {
    return moment(datetime).tz(Timezone.vi).format();
}
