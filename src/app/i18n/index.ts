import { en } from './en';
import { vi } from './vi';

export const Lang = { en, vi };
