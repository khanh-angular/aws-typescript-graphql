import { Pipe, PipeTransform } from '@angular/core';
import { I18n } from 'aws-amplify';

@Pipe({
  name: 'lang',
})

export class LangPipe implements PipeTransform {

  transform(value: any, ...args: any[]): any {
    if (value) {
      return I18n.get(value);
    }
    return null;
  }
}
