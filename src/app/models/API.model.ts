/* tslint:disable */
//  This file was automatically generated and should not be edited.

export type CreateUserInput = {
  id?: string | null,
  username: string,
  user_mode: Mode,
  fullname: string,
  avatar?: string | null,
  online_status?: OnlineStatus | null,
  friendly_search?: string | null,
  is_found_in_search?: boolean | null,
  location?: LocationInput | null,
  likes?: Array< string | null > | null,
};

export enum Mode {
  PUBLIC = "PUBLIC",
  PRIVATE = "PRIVATE",
}


export enum OnlineStatus {
  ONLINE = "ONLINE",
  OFFLINE = "OFFLINE",
  SLEEP = "SLEEP",
}


export type LocationInput = {
  lat: number,
  lng: number,
};

export type UpdateUserInput = {
  id: string,
  username?: string | null,
  user_mode?: Mode | null,
  fullname?: string | null,
  avatar?: string | null,
  online_status?: OnlineStatus | null,
  friendly_search?: string | null,
  is_found_in_search?: boolean | null,
  location?: LocationInput | null,
  likes?: Array< string | null > | null,
};

export type DeleteUserInput = {
  id?: string | null,
};

export type CreatePageInput = {
  id?: string | null,
  page_name: string,
  page_mode: Mode,
  page_type: PageTypeCategory,
  current_members_count?: number | null,
  limit_members_count?: number | null,
  is_freedom?: boolean | null,
  is_found_in_search?: boolean | null,
  is_found_in_profile?: boolean | null,
  page_ref_user_createdId: string,
};

export enum PageTypeCategory {
  SCHOOL = "SCHOOL",
  ROOM = "ROOM",
  GROUP = "GROUP",
  TEAM = "TEAM",
}


export type UpdatePageInput = {
  id: string,
  page_name?: string | null,
  page_mode?: Mode | null,
  page_type?: PageTypeCategory | null,
  current_members_count?: number | null,
  limit_members_count?: number | null,
  is_freedom?: boolean | null,
  is_found_in_search?: boolean | null,
  is_found_in_profile?: boolean | null,
  page_ref_user_createdId?: string | null,
};

export type DeletePageInput = {
  id?: string | null,
};

export type CreatePageMemberInput = {
  pageId_userId: string,
  score_count?: number | null,
  is_confirmed?: boolean | null,
  pageMember_pageId: string,
  pageMember_userId: string,
};

export type UpdatePageMemberInput = {
  pageId_userId: string,
  score_count?: number | null,
  is_confirmed?: boolean | null,
  pageMember_pageId?: string | null,
  pageMember_userId?: string | null,
};

export type DeletePageMemberInput = {
  pageId_userId: string,
};

export type CreatePostInput = {
  id?: string | null,
  content: string,
  files?: Array< string | null > | null,
  post_pageId: string,
  post_ref_user_createdId: string,
};

export type UpdatePostInput = {
  id: string,
  content?: string | null,
  files?: Array< string | null > | null,
  post_pageId?: string | null,
  post_ref_user_createdId?: string | null,
};

export type DeletePostInput = {
  id?: string | null,
};

export type CreateCommentInput = {
  id?: string | null,
  content: string,
  datetime_created: string,
  comment_ref_postId: string,
  comment_ref_userId: string,
};

export type UpdateCommentInput = {
  id: string,
  content?: string | null,
  datetime_created?: string | null,
  comment_ref_postId?: string | null,
  comment_ref_userId?: string | null,
};

export type DeleteCommentInput = {
  id?: string | null,
};

export type CreateLikeInput = {
  postId_userId: string,
  likePostId: string,
  like_ref_userId: string,
};

export type UpdateLikeInput = {
  postId_userId: string,
  likePostId?: string | null,
  like_ref_userId?: string | null,
};

export type DeleteLikeInput = {
  postId_userId: string,
};

export type CreateDocumentInput = {
  id?: string | null,
  doc_title: string,
  deadline?: string | null,
  doc_type: DocumentType,
  doc_questions?: Array< DocumentContentInput | null > | null,
  document_ref_user_createdId: string,
};

export enum DocumentType {
  QUIZZ = "QUIZZ",
  LETTERS = "LETTERS",
}


export type DocumentContentInput = {
  question_id: string,
  question: string,
  answers?: Array< DocumentAnswerInput | null > | null,
  option?: DocumentOptionInput | null,
};

export type DocumentAnswerInput = {
  order_id: string,
  value?: string | null,
  img?: string | null,
};

export type DocumentOptionInput = {
  video?: string | null,
  audio?: string | null,
  imgs?: Array< string | null > | null,
  score?: number | null,
  seconds?: number | null,
};

export type UpdateDocumentInput = {
  id: string,
  doc_title?: string | null,
  deadline?: string | null,
  doc_type?: DocumentType | null,
  doc_questions?: Array< DocumentContentInput | null > | null,
  document_ref_user_createdId?: string | null,
};

export type DeleteDocumentInput = {
  id?: string | null,
};

export type CreateMemberInDocumentInput = {
  id?: string | null,
  answers?: Array< DocumentAnswerOfMemberInput | null > | null,
  memberInDocument_ref_docId: string,
  memberInDocument_ref_userId: string,
};

export type DocumentAnswerOfMemberInput = {
  question_id: string,
  answer_ids?: Array< string > | null,
};

export type UpdateMemberInDocumentInput = {
  id: string,
  answers?: Array< DocumentAnswerOfMemberInput | null > | null,
  memberInDocument_ref_docId?: string | null,
  memberInDocument_ref_userId?: string | null,
};

export type DeleteMemberInDocumentInput = {
  id?: string | null,
};

export type CreateScheduleInput = {
  datetime_created?: string | null,
  datetime_expired?: string | null,
  datetime_start?: string | null,
  datetime_finish?: string | null,
  day_of_week?: string | null,
  day_of_year?: string | null,
  month_of_year?: string | null,
};

export type UpdateScheduleInput = {
  datetime_created?: string | null,
  datetime_expired?: string | null,
  datetime_start?: string | null,
  datetime_finish?: string | null,
  day_of_week?: string | null,
  day_of_year?: string | null,
  month_of_year?: string | null,
};

export type DeleteScheduleInput = {
  id?: string | null,
};

export type ModelUserFilterInput = {
  id?: ModelIDFilterInput | null,
  username?: ModelStringFilterInput | null,
  user_mode?: ModelModeFilterInput | null,
  fullname?: ModelStringFilterInput | null,
  avatar?: ModelStringFilterInput | null,
  online_status?: ModelOnlineStatusFilterInput | null,
  friendly_search?: ModelStringFilterInput | null,
  is_found_in_search?: ModelBooleanFilterInput | null,
  likes?: ModelStringFilterInput | null,
  and?: Array< ModelUserFilterInput | null > | null,
  or?: Array< ModelUserFilterInput | null > | null,
  not?: ModelUserFilterInput | null,
};

export type ModelIDFilterInput = {
  ne?: string | null,
  eq?: string | null,
  le?: string | null,
  lt?: string | null,
  ge?: string | null,
  gt?: string | null,
  contains?: string | null,
  notContains?: string | null,
  between?: Array< string | null > | null,
  beginsWith?: string | null,
};

export type ModelStringFilterInput = {
  ne?: string | null,
  eq?: string | null,
  le?: string | null,
  lt?: string | null,
  ge?: string | null,
  gt?: string | null,
  contains?: string | null,
  notContains?: string | null,
  between?: Array< string | null > | null,
  beginsWith?: string | null,
};

export type ModelModeFilterInput = {
  eq?: Mode | null,
  ne?: Mode | null,
};

export type ModelOnlineStatusFilterInput = {
  eq?: OnlineStatus | null,
  ne?: OnlineStatus | null,
};

export type ModelBooleanFilterInput = {
  ne?: boolean | null,
  eq?: boolean | null,
};

export type ModelPageFilterInput = {
  id?: ModelIDFilterInput | null,
  page_name?: ModelStringFilterInput | null,
  page_mode?: ModelModeFilterInput | null,
  page_type?: ModelPageTypeCategoryFilterInput | null,
  current_members_count?: ModelIntFilterInput | null,
  limit_members_count?: ModelIntFilterInput | null,
  is_freedom?: ModelBooleanFilterInput | null,
  is_found_in_search?: ModelBooleanFilterInput | null,
  is_found_in_profile?: ModelBooleanFilterInput | null,
  and?: Array< ModelPageFilterInput | null > | null,
  or?: Array< ModelPageFilterInput | null > | null,
  not?: ModelPageFilterInput | null,
};

export type ModelPageTypeCategoryFilterInput = {
  eq?: PageTypeCategory | null,
  ne?: PageTypeCategory | null,
};

export type ModelIntFilterInput = {
  ne?: number | null,
  eq?: number | null,
  le?: number | null,
  lt?: number | null,
  ge?: number | null,
  gt?: number | null,
  contains?: number | null,
  notContains?: number | null,
  between?: Array< number | null > | null,
};

export type ModelPageMemberFilterInput = {
  pageId_userId?: ModelIDFilterInput | null,
  score_count?: ModelIntFilterInput | null,
  is_confirmed?: ModelBooleanFilterInput | null,
  and?: Array< ModelPageMemberFilterInput | null > | null,
  or?: Array< ModelPageMemberFilterInput | null > | null,
  not?: ModelPageMemberFilterInput | null,
};

export enum ModelSortDirection {
  ASC = "ASC",
  DESC = "DESC",
}


export type ModelPostFilterInput = {
  id?: ModelIDFilterInput | null,
  content?: ModelStringFilterInput | null,
  files?: ModelStringFilterInput | null,
  and?: Array< ModelPostFilterInput | null > | null,
  or?: Array< ModelPostFilterInput | null > | null,
  not?: ModelPostFilterInput | null,
};

export type ModelCommentFilterInput = {
  id?: ModelIDFilterInput | null,
  content?: ModelStringFilterInput | null,
  datetime_created?: ModelStringFilterInput | null,
  and?: Array< ModelCommentFilterInput | null > | null,
  or?: Array< ModelCommentFilterInput | null > | null,
  not?: ModelCommentFilterInput | null,
};

export type ModelLikeFilterInput = {
  postId_userId?: ModelIDFilterInput | null,
  and?: Array< ModelLikeFilterInput | null > | null,
  or?: Array< ModelLikeFilterInput | null > | null,
  not?: ModelLikeFilterInput | null,
};

export type ModelDocumentFilterInput = {
  id?: ModelIDFilterInput | null,
  doc_title?: ModelStringFilterInput | null,
  deadline?: ModelStringFilterInput | null,
  doc_type?: ModelDocumentTypeFilterInput | null,
  and?: Array< ModelDocumentFilterInput | null > | null,
  or?: Array< ModelDocumentFilterInput | null > | null,
  not?: ModelDocumentFilterInput | null,
};

export type ModelDocumentTypeFilterInput = {
  eq?: DocumentType | null,
  ne?: DocumentType | null,
};

export type ModelMemberInDocumentFilterInput = {
  id?: ModelIDFilterInput | null,
  and?: Array< ModelMemberInDocumentFilterInput | null > | null,
  or?: Array< ModelMemberInDocumentFilterInput | null > | null,
  not?: ModelMemberInDocumentFilterInput | null,
};

export type ModelScheduleFilterInput = {
  datetime_created?: ModelStringFilterInput | null,
  datetime_expired?: ModelStringFilterInput | null,
  datetime_start?: ModelStringFilterInput | null,
  datetime_finish?: ModelStringFilterInput | null,
  day_of_week?: ModelStringFilterInput | null,
  day_of_year?: ModelStringFilterInput | null,
  month_of_year?: ModelStringFilterInput | null,
  and?: Array< ModelScheduleFilterInput | null > | null,
  or?: Array< ModelScheduleFilterInput | null > | null,
  not?: ModelScheduleFilterInput | null,
};

export type ModelStringKeyConditionInput = {
  eq?: string | null,
  le?: string | null,
  lt?: string | null,
  ge?: string | null,
  gt?: string | null,
  between?: Array< string | null > | null,
  beginsWith?: string | null,
};

export type SearchableUserFilterInput = {
  id?: SearchableIDFilterInput | null,
  username?: SearchableStringFilterInput | null,
  fullname?: SearchableStringFilterInput | null,
  avatar?: SearchableStringFilterInput | null,
  friendly_search?: SearchableStringFilterInput | null,
  is_found_in_search?: SearchableBooleanFilterInput | null,
  likes?: SearchableStringFilterInput | null,
  and?: Array< SearchableUserFilterInput | null > | null,
  or?: Array< SearchableUserFilterInput | null > | null,
  not?: SearchableUserFilterInput | null,
};

export type SearchableIDFilterInput = {
  ne?: string | null,
  eq?: string | null,
  match?: string | null,
  matchPhrase?: string | null,
  matchPhrasePrefix?: string | null,
  multiMatch?: string | null,
  exists?: boolean | null,
  wildcard?: string | null,
  regexp?: string | null,
};

export type SearchableStringFilterInput = {
  ne?: string | null,
  eq?: string | null,
  match?: string | null,
  matchPhrase?: string | null,
  matchPhrasePrefix?: string | null,
  multiMatch?: string | null,
  exists?: boolean | null,
  wildcard?: string | null,
  regexp?: string | null,
};

export type SearchableBooleanFilterInput = {
  eq?: boolean | null,
  ne?: boolean | null,
};

export type SearchableUserSortInput = {
  field?: SearchableUserSortableFields | null,
  direction?: SearchableSortDirection | null,
};

export enum SearchableUserSortableFields {
  id = "id",
  username = "username",
  fullname = "fullname",
  avatar = "avatar",
  friendly_search = "friendly_search",
  is_found_in_search = "is_found_in_search",
  likes = "likes",
}


export enum SearchableSortDirection {
  asc = "asc",
  desc = "desc",
}


export type SearchablePageFilterInput = {
  id?: SearchableIDFilterInput | null,
  page_name?: SearchableStringFilterInput | null,
  current_members_count?: SearchableIntFilterInput | null,
  limit_members_count?: SearchableIntFilterInput | null,
  is_freedom?: SearchableBooleanFilterInput | null,
  is_found_in_search?: SearchableBooleanFilterInput | null,
  is_found_in_profile?: SearchableBooleanFilterInput | null,
  and?: Array< SearchablePageFilterInput | null > | null,
  or?: Array< SearchablePageFilterInput | null > | null,
  not?: SearchablePageFilterInput | null,
};

export type SearchableIntFilterInput = {
  ne?: number | null,
  gt?: number | null,
  lt?: number | null,
  gte?: number | null,
  lte?: number | null,
  eq?: number | null,
  range?: Array< number | null > | null,
};

export type SearchablePageSortInput = {
  field?: SearchablePageSortableFields | null,
  direction?: SearchableSortDirection | null,
};

export enum SearchablePageSortableFields {
  id = "id",
  page_name = "page_name",
  current_members_count = "current_members_count",
  limit_members_count = "limit_members_count",
  is_freedom = "is_freedom",
  is_found_in_search = "is_found_in_search",
  is_found_in_profile = "is_found_in_profile",
}


export type CreateUserMutationVariables = {
  input: CreateUserInput,
};

export type CreateUserMutation = {
  createUser:  {
    __typename: "User",
    id: string,
    username: string,
    user_mode: Mode,
    fullname: string,
    avatar: string | null,
    online_status: OnlineStatus | null,
    friendly_search: string | null,
    is_found_in_search: boolean | null,
    location:  {
      __typename: "Location",
      lat: number,
      lng: number,
    } | null,
    likes: Array< string | null > | null,
    pages_created:  {
      __typename: "ModelPageConnection",
      nextToken: string | null,
    } | null,
    pages_joined:  {
      __typename: "ModelPageMemberConnection",
      nextToken: string | null,
    } | null,
    documents_created:  {
      __typename: "ModelDocumentConnection",
      nextToken: string | null,
    } | null,
    documents_joined:  {
      __typename: "ModelMemberInDocumentConnection",
      nextToken: string | null,
    } | null,
  } | null,
};

export type UpdateUserMutationVariables = {
  input: UpdateUserInput,
};

export type UpdateUserMutation = {
  updateUser:  {
    __typename: "User",
    id: string,
    username: string,
    user_mode: Mode,
    fullname: string,
    avatar: string | null,
    online_status: OnlineStatus | null,
    friendly_search: string | null,
    is_found_in_search: boolean | null,
    location:  {
      __typename: "Location",
      lat: number,
      lng: number,
    } | null,
    likes: Array< string | null > | null,
    pages_created:  {
      __typename: "ModelPageConnection",
      nextToken: string | null,
    } | null,
    pages_joined:  {
      __typename: "ModelPageMemberConnection",
      nextToken: string | null,
    } | null,
    documents_created:  {
      __typename: "ModelDocumentConnection",
      nextToken: string | null,
    } | null,
    documents_joined:  {
      __typename: "ModelMemberInDocumentConnection",
      nextToken: string | null,
    } | null,
  } | null,
};

export type DeleteUserMutationVariables = {
  input: DeleteUserInput,
};

export type DeleteUserMutation = {
  deleteUser:  {
    __typename: "User",
    id: string,
    username: string,
    user_mode: Mode,
    fullname: string,
    avatar: string | null,
    online_status: OnlineStatus | null,
    friendly_search: string | null,
    is_found_in_search: boolean | null,
    location:  {
      __typename: "Location",
      lat: number,
      lng: number,
    } | null,
    likes: Array< string | null > | null,
    pages_created:  {
      __typename: "ModelPageConnection",
      nextToken: string | null,
    } | null,
    pages_joined:  {
      __typename: "ModelPageMemberConnection",
      nextToken: string | null,
    } | null,
    documents_created:  {
      __typename: "ModelDocumentConnection",
      nextToken: string | null,
    } | null,
    documents_joined:  {
      __typename: "ModelMemberInDocumentConnection",
      nextToken: string | null,
    } | null,
  } | null,
};

export type CreatePageMutationVariables = {
  input: CreatePageInput,
};

export type CreatePageMutation = {
  createPage:  {
    __typename: "Page",
    id: string,
    page_name: string,
    page_mode: Mode,
    page_type: PageTypeCategory,
    current_members_count: number | null,
    limit_members_count: number | null,
    is_freedom: boolean | null,
    is_found_in_search: boolean | null,
    is_found_in_profile: boolean | null,
    schedule:  {
      __typename: "Schedule",
      datetime_created: string | null,
      datetime_expired: string | null,
      datetime_start: string | null,
      datetime_finish: string | null,
      day_of_week: string | null,
      day_of_year: string | null,
      month_of_year: string | null,
    } | null,
    members:  {
      __typename: "ModelPageMemberConnection",
      nextToken: string | null,
    } | null,
    posts:  {
      __typename: "ModelPostConnection",
      nextToken: string | null,
    } | null,
    _ref_user_created:  {
      __typename: "User",
      id: string,
      username: string,
      user_mode: Mode,
      fullname: string,
      avatar: string | null,
      online_status: OnlineStatus | null,
      friendly_search: string | null,
      is_found_in_search: boolean | null,
      likes: Array< string | null > | null,
    },
  } | null,
};

export type UpdatePageMutationVariables = {
  input: UpdatePageInput,
};

export type UpdatePageMutation = {
  updatePage:  {
    __typename: "Page",
    id: string,
    page_name: string,
    page_mode: Mode,
    page_type: PageTypeCategory,
    current_members_count: number | null,
    limit_members_count: number | null,
    is_freedom: boolean | null,
    is_found_in_search: boolean | null,
    is_found_in_profile: boolean | null,
    schedule:  {
      __typename: "Schedule",
      datetime_created: string | null,
      datetime_expired: string | null,
      datetime_start: string | null,
      datetime_finish: string | null,
      day_of_week: string | null,
      day_of_year: string | null,
      month_of_year: string | null,
    } | null,
    members:  {
      __typename: "ModelPageMemberConnection",
      nextToken: string | null,
    } | null,
    posts:  {
      __typename: "ModelPostConnection",
      nextToken: string | null,
    } | null,
    _ref_user_created:  {
      __typename: "User",
      id: string,
      username: string,
      user_mode: Mode,
      fullname: string,
      avatar: string | null,
      online_status: OnlineStatus | null,
      friendly_search: string | null,
      is_found_in_search: boolean | null,
      likes: Array< string | null > | null,
    },
  } | null,
};

export type DeletePageMutationVariables = {
  input: DeletePageInput,
};

export type DeletePageMutation = {
  deletePage:  {
    __typename: "Page",
    id: string,
    page_name: string,
    page_mode: Mode,
    page_type: PageTypeCategory,
    current_members_count: number | null,
    limit_members_count: number | null,
    is_freedom: boolean | null,
    is_found_in_search: boolean | null,
    is_found_in_profile: boolean | null,
    schedule:  {
      __typename: "Schedule",
      datetime_created: string | null,
      datetime_expired: string | null,
      datetime_start: string | null,
      datetime_finish: string | null,
      day_of_week: string | null,
      day_of_year: string | null,
      month_of_year: string | null,
    } | null,
    members:  {
      __typename: "ModelPageMemberConnection",
      nextToken: string | null,
    } | null,
    posts:  {
      __typename: "ModelPostConnection",
      nextToken: string | null,
    } | null,
    _ref_user_created:  {
      __typename: "User",
      id: string,
      username: string,
      user_mode: Mode,
      fullname: string,
      avatar: string | null,
      online_status: OnlineStatus | null,
      friendly_search: string | null,
      is_found_in_search: boolean | null,
      likes: Array< string | null > | null,
    },
  } | null,
};

export type CreatePageMemberMutationVariables = {
  input: CreatePageMemberInput,
};

export type CreatePageMemberMutation = {
  createPageMember:  {
    __typename: "PageMember",
    pageId_userId: string,
    score_count: number | null,
    is_confirmed: boolean | null,
    _page:  {
      __typename: "Page",
      id: string,
      page_name: string,
      page_mode: Mode,
      page_type: PageTypeCategory,
      current_members_count: number | null,
      limit_members_count: number | null,
      is_freedom: boolean | null,
      is_found_in_search: boolean | null,
      is_found_in_profile: boolean | null,
    },
    _user:  {
      __typename: "User",
      id: string,
      username: string,
      user_mode: Mode,
      fullname: string,
      avatar: string | null,
      online_status: OnlineStatus | null,
      friendly_search: string | null,
      is_found_in_search: boolean | null,
      likes: Array< string | null > | null,
    },
  } | null,
};

export type UpdatePageMemberMutationVariables = {
  input: UpdatePageMemberInput,
};

export type UpdatePageMemberMutation = {
  updatePageMember:  {
    __typename: "PageMember",
    pageId_userId: string,
    score_count: number | null,
    is_confirmed: boolean | null,
    _page:  {
      __typename: "Page",
      id: string,
      page_name: string,
      page_mode: Mode,
      page_type: PageTypeCategory,
      current_members_count: number | null,
      limit_members_count: number | null,
      is_freedom: boolean | null,
      is_found_in_search: boolean | null,
      is_found_in_profile: boolean | null,
    },
    _user:  {
      __typename: "User",
      id: string,
      username: string,
      user_mode: Mode,
      fullname: string,
      avatar: string | null,
      online_status: OnlineStatus | null,
      friendly_search: string | null,
      is_found_in_search: boolean | null,
      likes: Array< string | null > | null,
    },
  } | null,
};

export type DeletePageMemberMutationVariables = {
  input: DeletePageMemberInput,
};

export type DeletePageMemberMutation = {
  deletePageMember:  {
    __typename: "PageMember",
    pageId_userId: string,
    score_count: number | null,
    is_confirmed: boolean | null,
    _page:  {
      __typename: "Page",
      id: string,
      page_name: string,
      page_mode: Mode,
      page_type: PageTypeCategory,
      current_members_count: number | null,
      limit_members_count: number | null,
      is_freedom: boolean | null,
      is_found_in_search: boolean | null,
      is_found_in_profile: boolean | null,
    },
    _user:  {
      __typename: "User",
      id: string,
      username: string,
      user_mode: Mode,
      fullname: string,
      avatar: string | null,
      online_status: OnlineStatus | null,
      friendly_search: string | null,
      is_found_in_search: boolean | null,
      likes: Array< string | null > | null,
    },
  } | null,
};

export type CreatePostMutationVariables = {
  input: CreatePostInput,
};

export type CreatePostMutation = {
  createPost:  {
    __typename: "Post",
    id: string,
    content: string,
    files: Array< string | null > | null,
    likes:  {
      __typename: "ModelLikeConnection",
      nextToken: string | null,
    } | null,
    comments:  {
      __typename: "ModelCommentConnection",
      nextToken: string | null,
    } | null,
    _page:  {
      __typename: "Page",
      id: string,
      page_name: string,
      page_mode: Mode,
      page_type: PageTypeCategory,
      current_members_count: number | null,
      limit_members_count: number | null,
      is_freedom: boolean | null,
      is_found_in_search: boolean | null,
      is_found_in_profile: boolean | null,
    },
    _ref_user_created:  {
      __typename: "User",
      id: string,
      username: string,
      user_mode: Mode,
      fullname: string,
      avatar: string | null,
      online_status: OnlineStatus | null,
      friendly_search: string | null,
      is_found_in_search: boolean | null,
      likes: Array< string | null > | null,
    },
  } | null,
};

export type UpdatePostMutationVariables = {
  input: UpdatePostInput,
};

export type UpdatePostMutation = {
  updatePost:  {
    __typename: "Post",
    id: string,
    content: string,
    files: Array< string | null > | null,
    likes:  {
      __typename: "ModelLikeConnection",
      nextToken: string | null,
    } | null,
    comments:  {
      __typename: "ModelCommentConnection",
      nextToken: string | null,
    } | null,
    _page:  {
      __typename: "Page",
      id: string,
      page_name: string,
      page_mode: Mode,
      page_type: PageTypeCategory,
      current_members_count: number | null,
      limit_members_count: number | null,
      is_freedom: boolean | null,
      is_found_in_search: boolean | null,
      is_found_in_profile: boolean | null,
    },
    _ref_user_created:  {
      __typename: "User",
      id: string,
      username: string,
      user_mode: Mode,
      fullname: string,
      avatar: string | null,
      online_status: OnlineStatus | null,
      friendly_search: string | null,
      is_found_in_search: boolean | null,
      likes: Array< string | null > | null,
    },
  } | null,
};

export type DeletePostMutationVariables = {
  input: DeletePostInput,
};

export type DeletePostMutation = {
  deletePost:  {
    __typename: "Post",
    id: string,
    content: string,
    files: Array< string | null > | null,
    likes:  {
      __typename: "ModelLikeConnection",
      nextToken: string | null,
    } | null,
    comments:  {
      __typename: "ModelCommentConnection",
      nextToken: string | null,
    } | null,
    _page:  {
      __typename: "Page",
      id: string,
      page_name: string,
      page_mode: Mode,
      page_type: PageTypeCategory,
      current_members_count: number | null,
      limit_members_count: number | null,
      is_freedom: boolean | null,
      is_found_in_search: boolean | null,
      is_found_in_profile: boolean | null,
    },
    _ref_user_created:  {
      __typename: "User",
      id: string,
      username: string,
      user_mode: Mode,
      fullname: string,
      avatar: string | null,
      online_status: OnlineStatus | null,
      friendly_search: string | null,
      is_found_in_search: boolean | null,
      likes: Array< string | null > | null,
    },
  } | null,
};

export type CreateCommentMutationVariables = {
  input: CreateCommentInput,
};

export type CreateCommentMutation = {
  createComment:  {
    __typename: "Comment",
    id: string,
    content: string,
    datetime_created: string,
    _ref_post:  {
      __typename: "Post",
      id: string,
      content: string,
      files: Array< string | null > | null,
    },
    _ref_user:  {
      __typename: "User",
      id: string,
      username: string,
      user_mode: Mode,
      fullname: string,
      avatar: string | null,
      online_status: OnlineStatus | null,
      friendly_search: string | null,
      is_found_in_search: boolean | null,
      likes: Array< string | null > | null,
    },
  } | null,
};

export type UpdateCommentMutationVariables = {
  input: UpdateCommentInput,
};

export type UpdateCommentMutation = {
  updateComment:  {
    __typename: "Comment",
    id: string,
    content: string,
    datetime_created: string,
    _ref_post:  {
      __typename: "Post",
      id: string,
      content: string,
      files: Array< string | null > | null,
    },
    _ref_user:  {
      __typename: "User",
      id: string,
      username: string,
      user_mode: Mode,
      fullname: string,
      avatar: string | null,
      online_status: OnlineStatus | null,
      friendly_search: string | null,
      is_found_in_search: boolean | null,
      likes: Array< string | null > | null,
    },
  } | null,
};

export type DeleteCommentMutationVariables = {
  input: DeleteCommentInput,
};

export type DeleteCommentMutation = {
  deleteComment:  {
    __typename: "Comment",
    id: string,
    content: string,
    datetime_created: string,
    _ref_post:  {
      __typename: "Post",
      id: string,
      content: string,
      files: Array< string | null > | null,
    },
    _ref_user:  {
      __typename: "User",
      id: string,
      username: string,
      user_mode: Mode,
      fullname: string,
      avatar: string | null,
      online_status: OnlineStatus | null,
      friendly_search: string | null,
      is_found_in_search: boolean | null,
      likes: Array< string | null > | null,
    },
  } | null,
};

export type CreateLikeMutationVariables = {
  input: CreateLikeInput,
};

export type CreateLikeMutation = {
  createLike:  {
    __typename: "Like",
    postId_userId: string,
    post:  {
      __typename: "Post",
      id: string,
      content: string,
      files: Array< string | null > | null,
    },
    _ref_user:  {
      __typename: "User",
      id: string,
      username: string,
      user_mode: Mode,
      fullname: string,
      avatar: string | null,
      online_status: OnlineStatus | null,
      friendly_search: string | null,
      is_found_in_search: boolean | null,
      likes: Array< string | null > | null,
    },
  } | null,
};

export type UpdateLikeMutationVariables = {
  input: UpdateLikeInput,
};

export type UpdateLikeMutation = {
  updateLike:  {
    __typename: "Like",
    postId_userId: string,
    post:  {
      __typename: "Post",
      id: string,
      content: string,
      files: Array< string | null > | null,
    },
    _ref_user:  {
      __typename: "User",
      id: string,
      username: string,
      user_mode: Mode,
      fullname: string,
      avatar: string | null,
      online_status: OnlineStatus | null,
      friendly_search: string | null,
      is_found_in_search: boolean | null,
      likes: Array< string | null > | null,
    },
  } | null,
};

export type DeleteLikeMutationVariables = {
  input: DeleteLikeInput,
};

export type DeleteLikeMutation = {
  deleteLike:  {
    __typename: "Like",
    postId_userId: string,
    post:  {
      __typename: "Post",
      id: string,
      content: string,
      files: Array< string | null > | null,
    },
    _ref_user:  {
      __typename: "User",
      id: string,
      username: string,
      user_mode: Mode,
      fullname: string,
      avatar: string | null,
      online_status: OnlineStatus | null,
      friendly_search: string | null,
      is_found_in_search: boolean | null,
      likes: Array< string | null > | null,
    },
  } | null,
};

export type CreateDocumentMutationVariables = {
  input: CreateDocumentInput,
};

export type CreateDocumentMutation = {
  createDocument:  {
    __typename: "Document",
    id: string,
    doc_title: string,
    deadline: string | null,
    doc_type: DocumentType,
    doc_questions:  Array< {
      __typename: "DocumentContent",
      question_id: string,
      question: string,
    } | null > | null,
    _ref_user_created:  {
      __typename: "User",
      id: string,
      username: string,
      user_mode: Mode,
      fullname: string,
      avatar: string | null,
      online_status: OnlineStatus | null,
      friendly_search: string | null,
      is_found_in_search: boolean | null,
      likes: Array< string | null > | null,
    },
    _ref_members_joined:  {
      __typename: "ModelMemberInDocumentConnection",
      nextToken: string | null,
    } | null,
  } | null,
};

export type UpdateDocumentMutationVariables = {
  input: UpdateDocumentInput,
};

export type UpdateDocumentMutation = {
  updateDocument:  {
    __typename: "Document",
    id: string,
    doc_title: string,
    deadline: string | null,
    doc_type: DocumentType,
    doc_questions:  Array< {
      __typename: "DocumentContent",
      question_id: string,
      question: string,
    } | null > | null,
    _ref_user_created:  {
      __typename: "User",
      id: string,
      username: string,
      user_mode: Mode,
      fullname: string,
      avatar: string | null,
      online_status: OnlineStatus | null,
      friendly_search: string | null,
      is_found_in_search: boolean | null,
      likes: Array< string | null > | null,
    },
    _ref_members_joined:  {
      __typename: "ModelMemberInDocumentConnection",
      nextToken: string | null,
    } | null,
  } | null,
};

export type DeleteDocumentMutationVariables = {
  input: DeleteDocumentInput,
};

export type DeleteDocumentMutation = {
  deleteDocument:  {
    __typename: "Document",
    id: string,
    doc_title: string,
    deadline: string | null,
    doc_type: DocumentType,
    doc_questions:  Array< {
      __typename: "DocumentContent",
      question_id: string,
      question: string,
    } | null > | null,
    _ref_user_created:  {
      __typename: "User",
      id: string,
      username: string,
      user_mode: Mode,
      fullname: string,
      avatar: string | null,
      online_status: OnlineStatus | null,
      friendly_search: string | null,
      is_found_in_search: boolean | null,
      likes: Array< string | null > | null,
    },
    _ref_members_joined:  {
      __typename: "ModelMemberInDocumentConnection",
      nextToken: string | null,
    } | null,
  } | null,
};

export type CreateMemberInDocumentMutationVariables = {
  input: CreateMemberInDocumentInput,
};

export type CreateMemberInDocumentMutation = {
  createMemberInDocument:  {
    __typename: "MemberInDocument",
    id: string,
    answers:  Array< {
      __typename: "DocumentAnswerOfMember",
      question_id: string,
      answer_ids: Array< string > | null,
    } | null > | null,
    _ref_doc:  {
      __typename: "Document",
      id: string,
      doc_title: string,
      deadline: string | null,
      doc_type: DocumentType,
    },
    _ref_user:  {
      __typename: "User",
      id: string,
      username: string,
      user_mode: Mode,
      fullname: string,
      avatar: string | null,
      online_status: OnlineStatus | null,
      friendly_search: string | null,
      is_found_in_search: boolean | null,
      likes: Array< string | null > | null,
    },
  } | null,
};

export type UpdateMemberInDocumentMutationVariables = {
  input: UpdateMemberInDocumentInput,
};

export type UpdateMemberInDocumentMutation = {
  updateMemberInDocument:  {
    __typename: "MemberInDocument",
    id: string,
    answers:  Array< {
      __typename: "DocumentAnswerOfMember",
      question_id: string,
      answer_ids: Array< string > | null,
    } | null > | null,
    _ref_doc:  {
      __typename: "Document",
      id: string,
      doc_title: string,
      deadline: string | null,
      doc_type: DocumentType,
    },
    _ref_user:  {
      __typename: "User",
      id: string,
      username: string,
      user_mode: Mode,
      fullname: string,
      avatar: string | null,
      online_status: OnlineStatus | null,
      friendly_search: string | null,
      is_found_in_search: boolean | null,
      likes: Array< string | null > | null,
    },
  } | null,
};

export type DeleteMemberInDocumentMutationVariables = {
  input: DeleteMemberInDocumentInput,
};

export type DeleteMemberInDocumentMutation = {
  deleteMemberInDocument:  {
    __typename: "MemberInDocument",
    id: string,
    answers:  Array< {
      __typename: "DocumentAnswerOfMember",
      question_id: string,
      answer_ids: Array< string > | null,
    } | null > | null,
    _ref_doc:  {
      __typename: "Document",
      id: string,
      doc_title: string,
      deadline: string | null,
      doc_type: DocumentType,
    },
    _ref_user:  {
      __typename: "User",
      id: string,
      username: string,
      user_mode: Mode,
      fullname: string,
      avatar: string | null,
      online_status: OnlineStatus | null,
      friendly_search: string | null,
      is_found_in_search: boolean | null,
      likes: Array< string | null > | null,
    },
  } | null,
};

export type CreateScheduleMutationVariables = {
  input: CreateScheduleInput,
};

export type CreateScheduleMutation = {
  createSchedule:  {
    __typename: "Schedule",
    datetime_created: string | null,
    datetime_expired: string | null,
    datetime_start: string | null,
    datetime_finish: string | null,
    day_of_week: string | null,
    day_of_year: string | null,
    month_of_year: string | null,
  } | null,
};

export type UpdateScheduleMutationVariables = {
  input: UpdateScheduleInput,
};

export type UpdateScheduleMutation = {
  updateSchedule:  {
    __typename: "Schedule",
    datetime_created: string | null,
    datetime_expired: string | null,
    datetime_start: string | null,
    datetime_finish: string | null,
    day_of_week: string | null,
    day_of_year: string | null,
    month_of_year: string | null,
  } | null,
};

export type DeleteScheduleMutationVariables = {
  input: DeleteScheduleInput,
};

export type DeleteScheduleMutation = {
  deleteSchedule:  {
    __typename: "Schedule",
    datetime_created: string | null,
    datetime_expired: string | null,
    datetime_start: string | null,
    datetime_finish: string | null,
    day_of_week: string | null,
    day_of_year: string | null,
    month_of_year: string | null,
  } | null,
};

export type UsersNearByMeQueryVariables = {
  location: LocationInput,
  km: number,
};

export type UsersNearByMeQuery = {
  usersNearByMe:  {
    __typename: "User",
    id: string,
    username: string,
    user_mode: Mode,
    fullname: string,
    avatar: string | null,
    online_status: OnlineStatus | null,
    friendly_search: string | null,
    is_found_in_search: boolean | null,
    location:  {
      __typename: "Location",
      lat: number,
      lng: number,
    } | null,
    likes: Array< string | null > | null,
    pages_created:  {
      __typename: "ModelPageConnection",
      nextToken: string | null,
    } | null,
    pages_joined:  {
      __typename: "ModelPageMemberConnection",
      nextToken: string | null,
    } | null,
    documents_created:  {
      __typename: "ModelDocumentConnection",
      nextToken: string | null,
    } | null,
    documents_joined:  {
      __typename: "ModelMemberInDocumentConnection",
      nextToken: string | null,
    } | null,
  } | null,
};

export type GetUserQueryVariables = {
  id: string,
};

export type GetUserQuery = {
  getUser:  {
    __typename: "User",
    id: string,
    username: string,
    user_mode: Mode,
    fullname: string,
    avatar: string | null,
    online_status: OnlineStatus | null,
    friendly_search: string | null,
    is_found_in_search: boolean | null,
    location:  {
      __typename: "Location",
      lat: number,
      lng: number,
    } | null,
    likes: Array< string | null > | null,
    pages_created:  {
      __typename: "ModelPageConnection",
      nextToken: string | null,
    } | null,
    pages_joined:  {
      __typename: "ModelPageMemberConnection",
      nextToken: string | null,
    } | null,
    documents_created:  {
      __typename: "ModelDocumentConnection",
      nextToken: string | null,
    } | null,
    documents_joined:  {
      __typename: "ModelMemberInDocumentConnection",
      nextToken: string | null,
    } | null,
  } | null,
};

export type ListUsersQueryVariables = {
  filter?: ModelUserFilterInput | null,
  limit?: number | null,
  nextToken?: string | null,
};

export type ListUsersQuery = {
  listUsers:  {
    __typename: "ModelUserConnection",
    items:  Array< {
      __typename: "User",
      id: string,
      username: string,
      user_mode: Mode,
      fullname: string,
      avatar: string | null,
      online_status: OnlineStatus | null,
      friendly_search: string | null,
      is_found_in_search: boolean | null,
      likes: Array< string | null > | null,
    } | null > | null,
    nextToken: string | null,
  } | null,
};

export type GetPageQueryVariables = {
  id: string,
};

export type GetPageQuery = {
  getPage:  {
    __typename: "Page",
    id: string,
    page_name: string,
    page_mode: Mode,
    page_type: PageTypeCategory,
    current_members_count: number | null,
    limit_members_count: number | null,
    is_freedom: boolean | null,
    is_found_in_search: boolean | null,
    is_found_in_profile: boolean | null,
    schedule:  {
      __typename: "Schedule",
      datetime_created: string | null,
      datetime_expired: string | null,
      datetime_start: string | null,
      datetime_finish: string | null,
      day_of_week: string | null,
      day_of_year: string | null,
      month_of_year: string | null,
    } | null,
    members:  {
      __typename: "ModelPageMemberConnection",
      nextToken: string | null,
    } | null,
    posts:  {
      __typename: "ModelPostConnection",
      nextToken: string | null,
    } | null,
    _ref_user_created:  {
      __typename: "User",
      id: string,
      username: string,
      user_mode: Mode,
      fullname: string,
      avatar: string | null,
      online_status: OnlineStatus | null,
      friendly_search: string | null,
      is_found_in_search: boolean | null,
      likes: Array< string | null > | null,
    },
  } | null,
};

export type ListPagesQueryVariables = {
  filter?: ModelPageFilterInput | null,
  limit?: number | null,
  nextToken?: string | null,
};

export type ListPagesQuery = {
  listPages:  {
    __typename: "ModelPageConnection",
    items:  Array< {
      __typename: "Page",
      id: string,
      page_name: string,
      page_mode: Mode,
      page_type: PageTypeCategory,
      current_members_count: number | null,
      limit_members_count: number | null,
      is_freedom: boolean | null,
      is_found_in_search: boolean | null,
      is_found_in_profile: boolean | null,
    } | null > | null,
    nextToken: string | null,
  } | null,
};

export type GetPageMemberQueryVariables = {
  pageId_userId: string,
};

export type GetPageMemberQuery = {
  getPageMember:  {
    __typename: "PageMember",
    pageId_userId: string,
    score_count: number | null,
    is_confirmed: boolean | null,
    _page:  {
      __typename: "Page",
      id: string,
      page_name: string,
      page_mode: Mode,
      page_type: PageTypeCategory,
      current_members_count: number | null,
      limit_members_count: number | null,
      is_freedom: boolean | null,
      is_found_in_search: boolean | null,
      is_found_in_profile: boolean | null,
    },
    _user:  {
      __typename: "User",
      id: string,
      username: string,
      user_mode: Mode,
      fullname: string,
      avatar: string | null,
      online_status: OnlineStatus | null,
      friendly_search: string | null,
      is_found_in_search: boolean | null,
      likes: Array< string | null > | null,
    },
  } | null,
};

export type ListPageMembersQueryVariables = {
  pageId_userId?: string | null,
  filter?: ModelPageMemberFilterInput | null,
  limit?: number | null,
  nextToken?: string | null,
  sortDirection?: ModelSortDirection | null,
};

export type ListPageMembersQuery = {
  listPageMembers:  {
    __typename: "ModelPageMemberConnection",
    items:  Array< {
      __typename: "PageMember",
      pageId_userId: string,
      score_count: number | null,
      is_confirmed: boolean | null,
    } | null > | null,
    nextToken: string | null,
  } | null,
};

export type GetPostQueryVariables = {
  id: string,
};

export type GetPostQuery = {
  getPost:  {
    __typename: "Post",
    id: string,
    content: string,
    files: Array< string | null > | null,
    likes:  {
      __typename: "ModelLikeConnection",
      nextToken: string | null,
    } | null,
    comments:  {
      __typename: "ModelCommentConnection",
      nextToken: string | null,
    } | null,
    _page:  {
      __typename: "Page",
      id: string,
      page_name: string,
      page_mode: Mode,
      page_type: PageTypeCategory,
      current_members_count: number | null,
      limit_members_count: number | null,
      is_freedom: boolean | null,
      is_found_in_search: boolean | null,
      is_found_in_profile: boolean | null,
    },
    _ref_user_created:  {
      __typename: "User",
      id: string,
      username: string,
      user_mode: Mode,
      fullname: string,
      avatar: string | null,
      online_status: OnlineStatus | null,
      friendly_search: string | null,
      is_found_in_search: boolean | null,
      likes: Array< string | null > | null,
    },
  } | null,
};

export type ListPostsQueryVariables = {
  filter?: ModelPostFilterInput | null,
  limit?: number | null,
  nextToken?: string | null,
};

export type ListPostsQuery = {
  listPosts:  {
    __typename: "ModelPostConnection",
    items:  Array< {
      __typename: "Post",
      id: string,
      content: string,
      files: Array< string | null > | null,
    } | null > | null,
    nextToken: string | null,
  } | null,
};

export type GetCommentQueryVariables = {
  id: string,
};

export type GetCommentQuery = {
  getComment:  {
    __typename: "Comment",
    id: string,
    content: string,
    datetime_created: string,
    _ref_post:  {
      __typename: "Post",
      id: string,
      content: string,
      files: Array< string | null > | null,
    },
    _ref_user:  {
      __typename: "User",
      id: string,
      username: string,
      user_mode: Mode,
      fullname: string,
      avatar: string | null,
      online_status: OnlineStatus | null,
      friendly_search: string | null,
      is_found_in_search: boolean | null,
      likes: Array< string | null > | null,
    },
  } | null,
};

export type ListCommentsQueryVariables = {
  filter?: ModelCommentFilterInput | null,
  limit?: number | null,
  nextToken?: string | null,
};

export type ListCommentsQuery = {
  listComments:  {
    __typename: "ModelCommentConnection",
    items:  Array< {
      __typename: "Comment",
      id: string,
      content: string,
      datetime_created: string,
    } | null > | null,
    nextToken: string | null,
  } | null,
};

export type GetLikeQueryVariables = {
  postId_userId: string,
};

export type GetLikeQuery = {
  getLike:  {
    __typename: "Like",
    postId_userId: string,
    post:  {
      __typename: "Post",
      id: string,
      content: string,
      files: Array< string | null > | null,
    },
    _ref_user:  {
      __typename: "User",
      id: string,
      username: string,
      user_mode: Mode,
      fullname: string,
      avatar: string | null,
      online_status: OnlineStatus | null,
      friendly_search: string | null,
      is_found_in_search: boolean | null,
      likes: Array< string | null > | null,
    },
  } | null,
};

export type ListLikesQueryVariables = {
  postId_userId?: string | null,
  filter?: ModelLikeFilterInput | null,
  limit?: number | null,
  nextToken?: string | null,
  sortDirection?: ModelSortDirection | null,
};

export type ListLikesQuery = {
  listLikes:  {
    __typename: "ModelLikeConnection",
    items:  Array< {
      __typename: "Like",
      postId_userId: string,
    } | null > | null,
    nextToken: string | null,
  } | null,
};

export type GetDocumentQueryVariables = {
  id: string,
};

export type GetDocumentQuery = {
  getDocument:  {
    __typename: "Document",
    id: string,
    doc_title: string,
    deadline: string | null,
    doc_type: DocumentType,
    doc_questions:  Array< {
      __typename: "DocumentContent",
      question_id: string,
      question: string,
    } | null > | null,
    _ref_user_created:  {
      __typename: "User",
      id: string,
      username: string,
      user_mode: Mode,
      fullname: string,
      avatar: string | null,
      online_status: OnlineStatus | null,
      friendly_search: string | null,
      is_found_in_search: boolean | null,
      likes: Array< string | null > | null,
    },
    _ref_members_joined:  {
      __typename: "ModelMemberInDocumentConnection",
      nextToken: string | null,
    } | null,
  } | null,
};

export type ListDocumentsQueryVariables = {
  filter?: ModelDocumentFilterInput | null,
  limit?: number | null,
  nextToken?: string | null,
};

export type ListDocumentsQuery = {
  listDocuments:  {
    __typename: "ModelDocumentConnection",
    items:  Array< {
      __typename: "Document",
      id: string,
      doc_title: string,
      deadline: string | null,
      doc_type: DocumentType,
    } | null > | null,
    nextToken: string | null,
  } | null,
};

export type GetMemberInDocumentQueryVariables = {
  id: string,
};

export type GetMemberInDocumentQuery = {
  getMemberInDocument:  {
    __typename: "MemberInDocument",
    id: string,
    answers:  Array< {
      __typename: "DocumentAnswerOfMember",
      question_id: string,
      answer_ids: Array< string > | null,
    } | null > | null,
    _ref_doc:  {
      __typename: "Document",
      id: string,
      doc_title: string,
      deadline: string | null,
      doc_type: DocumentType,
    },
    _ref_user:  {
      __typename: "User",
      id: string,
      username: string,
      user_mode: Mode,
      fullname: string,
      avatar: string | null,
      online_status: OnlineStatus | null,
      friendly_search: string | null,
      is_found_in_search: boolean | null,
      likes: Array< string | null > | null,
    },
  } | null,
};

export type ListMemberInDocumentsQueryVariables = {
  filter?: ModelMemberInDocumentFilterInput | null,
  limit?: number | null,
  nextToken?: string | null,
};

export type ListMemberInDocumentsQuery = {
  listMemberInDocuments:  {
    __typename: "ModelMemberInDocumentConnection",
    items:  Array< {
      __typename: "MemberInDocument",
      id: string,
    } | null > | null,
    nextToken: string | null,
  } | null,
};

export type GetScheduleQueryVariables = {
  id: string,
};

export type GetScheduleQuery = {
  getSchedule:  {
    __typename: "Schedule",
    datetime_created: string | null,
    datetime_expired: string | null,
    datetime_start: string | null,
    datetime_finish: string | null,
    day_of_week: string | null,
    day_of_year: string | null,
    month_of_year: string | null,
  } | null,
};

export type ListSchedulesQueryVariables = {
  filter?: ModelScheduleFilterInput | null,
  limit?: number | null,
  nextToken?: string | null,
};

export type ListSchedulesQuery = {
  listSchedules:  {
    __typename: "ModelScheduleConnection",
    items:  Array< {
      __typename: "Schedule",
      datetime_created: string | null,
      datetime_expired: string | null,
      datetime_start: string | null,
      datetime_finish: string | null,
      day_of_week: string | null,
      day_of_year: string | null,
      month_of_year: string | null,
    } | null > | null,
    nextToken: string | null,
  } | null,
};

export type ListPublicPagesQueryVariables = {
  page_type?: PageTypeCategory | null,
  page_mode?: ModelStringKeyConditionInput | null,
  sortDirection?: ModelSortDirection | null,
  filter?: ModelPageFilterInput | null,
  limit?: number | null,
  nextToken?: string | null,
};

export type ListPublicPagesQuery = {
  listPublicPages:  {
    __typename: "ModelPageConnection",
    items:  Array< {
      __typename: "Page",
      id: string,
      page_name: string,
      page_mode: Mode,
      page_type: PageTypeCategory,
      current_members_count: number | null,
      limit_members_count: number | null,
      is_freedom: boolean | null,
      is_found_in_search: boolean | null,
      is_found_in_profile: boolean | null,
    } | null > | null,
    nextToken: string | null,
  } | null,
};

export type SearchUsersQueryVariables = {
  filter?: SearchableUserFilterInput | null,
  sort?: SearchableUserSortInput | null,
  limit?: number | null,
  nextToken?: string | null,
};

export type SearchUsersQuery = {
  searchUsers:  {
    __typename: "SearchableUserConnection",
    items:  Array< {
      __typename: "User",
      id: string,
      username: string,
      user_mode: Mode,
      fullname: string,
      avatar: string | null,
      online_status: OnlineStatus | null,
      friendly_search: string | null,
      is_found_in_search: boolean | null,
      likes: Array< string | null > | null,
    } | null > | null,
    nextToken: string | null,
  } | null,
};

export type SearchPagesQueryVariables = {
  filter?: SearchablePageFilterInput | null,
  sort?: SearchablePageSortInput | null,
  limit?: number | null,
  nextToken?: string | null,
};

export type SearchPagesQuery = {
  searchPages:  {
    __typename: "SearchablePageConnection",
    items:  Array< {
      __typename: "Page",
      id: string,
      page_name: string,
      page_mode: Mode,
      page_type: PageTypeCategory,
      current_members_count: number | null,
      limit_members_count: number | null,
      is_freedom: boolean | null,
      is_found_in_search: boolean | null,
      is_found_in_profile: boolean | null,
    } | null > | null,
    nextToken: string | null,
  } | null,
};

export type OnCreateUserSubscription = {
  onCreateUser:  {
    __typename: "User",
    id: string,
    username: string,
    user_mode: Mode,
    fullname: string,
    avatar: string | null,
    online_status: OnlineStatus | null,
    friendly_search: string | null,
    is_found_in_search: boolean | null,
    location:  {
      __typename: "Location",
      lat: number,
      lng: number,
    } | null,
    likes: Array< string | null > | null,
    pages_created:  {
      __typename: "ModelPageConnection",
      nextToken: string | null,
    } | null,
    pages_joined:  {
      __typename: "ModelPageMemberConnection",
      nextToken: string | null,
    } | null,
    documents_created:  {
      __typename: "ModelDocumentConnection",
      nextToken: string | null,
    } | null,
    documents_joined:  {
      __typename: "ModelMemberInDocumentConnection",
      nextToken: string | null,
    } | null,
  } | null,
};

export type OnUpdateUserSubscription = {
  onUpdateUser:  {
    __typename: "User",
    id: string,
    username: string,
    user_mode: Mode,
    fullname: string,
    avatar: string | null,
    online_status: OnlineStatus | null,
    friendly_search: string | null,
    is_found_in_search: boolean | null,
    location:  {
      __typename: "Location",
      lat: number,
      lng: number,
    } | null,
    likes: Array< string | null > | null,
    pages_created:  {
      __typename: "ModelPageConnection",
      nextToken: string | null,
    } | null,
    pages_joined:  {
      __typename: "ModelPageMemberConnection",
      nextToken: string | null,
    } | null,
    documents_created:  {
      __typename: "ModelDocumentConnection",
      nextToken: string | null,
    } | null,
    documents_joined:  {
      __typename: "ModelMemberInDocumentConnection",
      nextToken: string | null,
    } | null,
  } | null,
};

export type OnDeleteUserSubscription = {
  onDeleteUser:  {
    __typename: "User",
    id: string,
    username: string,
    user_mode: Mode,
    fullname: string,
    avatar: string | null,
    online_status: OnlineStatus | null,
    friendly_search: string | null,
    is_found_in_search: boolean | null,
    location:  {
      __typename: "Location",
      lat: number,
      lng: number,
    } | null,
    likes: Array< string | null > | null,
    pages_created:  {
      __typename: "ModelPageConnection",
      nextToken: string | null,
    } | null,
    pages_joined:  {
      __typename: "ModelPageMemberConnection",
      nextToken: string | null,
    } | null,
    documents_created:  {
      __typename: "ModelDocumentConnection",
      nextToken: string | null,
    } | null,
    documents_joined:  {
      __typename: "ModelMemberInDocumentConnection",
      nextToken: string | null,
    } | null,
  } | null,
};

export type OnCreatePageSubscription = {
  onCreatePage:  {
    __typename: "Page",
    id: string,
    page_name: string,
    page_mode: Mode,
    page_type: PageTypeCategory,
    current_members_count: number | null,
    limit_members_count: number | null,
    is_freedom: boolean | null,
    is_found_in_search: boolean | null,
    is_found_in_profile: boolean | null,
    schedule:  {
      __typename: "Schedule",
      datetime_created: string | null,
      datetime_expired: string | null,
      datetime_start: string | null,
      datetime_finish: string | null,
      day_of_week: string | null,
      day_of_year: string | null,
      month_of_year: string | null,
    } | null,
    members:  {
      __typename: "ModelPageMemberConnection",
      nextToken: string | null,
    } | null,
    posts:  {
      __typename: "ModelPostConnection",
      nextToken: string | null,
    } | null,
    _ref_user_created:  {
      __typename: "User",
      id: string,
      username: string,
      user_mode: Mode,
      fullname: string,
      avatar: string | null,
      online_status: OnlineStatus | null,
      friendly_search: string | null,
      is_found_in_search: boolean | null,
      likes: Array< string | null > | null,
    },
  } | null,
};

export type OnUpdatePageSubscription = {
  onUpdatePage:  {
    __typename: "Page",
    id: string,
    page_name: string,
    page_mode: Mode,
    page_type: PageTypeCategory,
    current_members_count: number | null,
    limit_members_count: number | null,
    is_freedom: boolean | null,
    is_found_in_search: boolean | null,
    is_found_in_profile: boolean | null,
    schedule:  {
      __typename: "Schedule",
      datetime_created: string | null,
      datetime_expired: string | null,
      datetime_start: string | null,
      datetime_finish: string | null,
      day_of_week: string | null,
      day_of_year: string | null,
      month_of_year: string | null,
    } | null,
    members:  {
      __typename: "ModelPageMemberConnection",
      nextToken: string | null,
    } | null,
    posts:  {
      __typename: "ModelPostConnection",
      nextToken: string | null,
    } | null,
    _ref_user_created:  {
      __typename: "User",
      id: string,
      username: string,
      user_mode: Mode,
      fullname: string,
      avatar: string | null,
      online_status: OnlineStatus | null,
      friendly_search: string | null,
      is_found_in_search: boolean | null,
      likes: Array< string | null > | null,
    },
  } | null,
};

export type OnDeletePageSubscription = {
  onDeletePage:  {
    __typename: "Page",
    id: string,
    page_name: string,
    page_mode: Mode,
    page_type: PageTypeCategory,
    current_members_count: number | null,
    limit_members_count: number | null,
    is_freedom: boolean | null,
    is_found_in_search: boolean | null,
    is_found_in_profile: boolean | null,
    schedule:  {
      __typename: "Schedule",
      datetime_created: string | null,
      datetime_expired: string | null,
      datetime_start: string | null,
      datetime_finish: string | null,
      day_of_week: string | null,
      day_of_year: string | null,
      month_of_year: string | null,
    } | null,
    members:  {
      __typename: "ModelPageMemberConnection",
      nextToken: string | null,
    } | null,
    posts:  {
      __typename: "ModelPostConnection",
      nextToken: string | null,
    } | null,
    _ref_user_created:  {
      __typename: "User",
      id: string,
      username: string,
      user_mode: Mode,
      fullname: string,
      avatar: string | null,
      online_status: OnlineStatus | null,
      friendly_search: string | null,
      is_found_in_search: boolean | null,
      likes: Array< string | null > | null,
    },
  } | null,
};

export type OnCreatePageMemberSubscription = {
  onCreatePageMember:  {
    __typename: "PageMember",
    pageId_userId: string,
    score_count: number | null,
    is_confirmed: boolean | null,
    _page:  {
      __typename: "Page",
      id: string,
      page_name: string,
      page_mode: Mode,
      page_type: PageTypeCategory,
      current_members_count: number | null,
      limit_members_count: number | null,
      is_freedom: boolean | null,
      is_found_in_search: boolean | null,
      is_found_in_profile: boolean | null,
    },
    _user:  {
      __typename: "User",
      id: string,
      username: string,
      user_mode: Mode,
      fullname: string,
      avatar: string | null,
      online_status: OnlineStatus | null,
      friendly_search: string | null,
      is_found_in_search: boolean | null,
      likes: Array< string | null > | null,
    },
  } | null,
};

export type OnUpdatePageMemberSubscription = {
  onUpdatePageMember:  {
    __typename: "PageMember",
    pageId_userId: string,
    score_count: number | null,
    is_confirmed: boolean | null,
    _page:  {
      __typename: "Page",
      id: string,
      page_name: string,
      page_mode: Mode,
      page_type: PageTypeCategory,
      current_members_count: number | null,
      limit_members_count: number | null,
      is_freedom: boolean | null,
      is_found_in_search: boolean | null,
      is_found_in_profile: boolean | null,
    },
    _user:  {
      __typename: "User",
      id: string,
      username: string,
      user_mode: Mode,
      fullname: string,
      avatar: string | null,
      online_status: OnlineStatus | null,
      friendly_search: string | null,
      is_found_in_search: boolean | null,
      likes: Array< string | null > | null,
    },
  } | null,
};

export type OnDeletePageMemberSubscription = {
  onDeletePageMember:  {
    __typename: "PageMember",
    pageId_userId: string,
    score_count: number | null,
    is_confirmed: boolean | null,
    _page:  {
      __typename: "Page",
      id: string,
      page_name: string,
      page_mode: Mode,
      page_type: PageTypeCategory,
      current_members_count: number | null,
      limit_members_count: number | null,
      is_freedom: boolean | null,
      is_found_in_search: boolean | null,
      is_found_in_profile: boolean | null,
    },
    _user:  {
      __typename: "User",
      id: string,
      username: string,
      user_mode: Mode,
      fullname: string,
      avatar: string | null,
      online_status: OnlineStatus | null,
      friendly_search: string | null,
      is_found_in_search: boolean | null,
      likes: Array< string | null > | null,
    },
  } | null,
};

export type OnCreatePostSubscription = {
  onCreatePost:  {
    __typename: "Post",
    id: string,
    content: string,
    files: Array< string | null > | null,
    likes:  {
      __typename: "ModelLikeConnection",
      nextToken: string | null,
    } | null,
    comments:  {
      __typename: "ModelCommentConnection",
      nextToken: string | null,
    } | null,
    _page:  {
      __typename: "Page",
      id: string,
      page_name: string,
      page_mode: Mode,
      page_type: PageTypeCategory,
      current_members_count: number | null,
      limit_members_count: number | null,
      is_freedom: boolean | null,
      is_found_in_search: boolean | null,
      is_found_in_profile: boolean | null,
    },
    _ref_user_created:  {
      __typename: "User",
      id: string,
      username: string,
      user_mode: Mode,
      fullname: string,
      avatar: string | null,
      online_status: OnlineStatus | null,
      friendly_search: string | null,
      is_found_in_search: boolean | null,
      likes: Array< string | null > | null,
    },
  } | null,
};

export type OnUpdatePostSubscription = {
  onUpdatePost:  {
    __typename: "Post",
    id: string,
    content: string,
    files: Array< string | null > | null,
    likes:  {
      __typename: "ModelLikeConnection",
      nextToken: string | null,
    } | null,
    comments:  {
      __typename: "ModelCommentConnection",
      nextToken: string | null,
    } | null,
    _page:  {
      __typename: "Page",
      id: string,
      page_name: string,
      page_mode: Mode,
      page_type: PageTypeCategory,
      current_members_count: number | null,
      limit_members_count: number | null,
      is_freedom: boolean | null,
      is_found_in_search: boolean | null,
      is_found_in_profile: boolean | null,
    },
    _ref_user_created:  {
      __typename: "User",
      id: string,
      username: string,
      user_mode: Mode,
      fullname: string,
      avatar: string | null,
      online_status: OnlineStatus | null,
      friendly_search: string | null,
      is_found_in_search: boolean | null,
      likes: Array< string | null > | null,
    },
  } | null,
};

export type OnDeletePostSubscription = {
  onDeletePost:  {
    __typename: "Post",
    id: string,
    content: string,
    files: Array< string | null > | null,
    likes:  {
      __typename: "ModelLikeConnection",
      nextToken: string | null,
    } | null,
    comments:  {
      __typename: "ModelCommentConnection",
      nextToken: string | null,
    } | null,
    _page:  {
      __typename: "Page",
      id: string,
      page_name: string,
      page_mode: Mode,
      page_type: PageTypeCategory,
      current_members_count: number | null,
      limit_members_count: number | null,
      is_freedom: boolean | null,
      is_found_in_search: boolean | null,
      is_found_in_profile: boolean | null,
    },
    _ref_user_created:  {
      __typename: "User",
      id: string,
      username: string,
      user_mode: Mode,
      fullname: string,
      avatar: string | null,
      online_status: OnlineStatus | null,
      friendly_search: string | null,
      is_found_in_search: boolean | null,
      likes: Array< string | null > | null,
    },
  } | null,
};

export type OnCreateCommentSubscription = {
  onCreateComment:  {
    __typename: "Comment",
    id: string,
    content: string,
    datetime_created: string,
    _ref_post:  {
      __typename: "Post",
      id: string,
      content: string,
      files: Array< string | null > | null,
    },
    _ref_user:  {
      __typename: "User",
      id: string,
      username: string,
      user_mode: Mode,
      fullname: string,
      avatar: string | null,
      online_status: OnlineStatus | null,
      friendly_search: string | null,
      is_found_in_search: boolean | null,
      likes: Array< string | null > | null,
    },
  } | null,
};

export type OnUpdateCommentSubscription = {
  onUpdateComment:  {
    __typename: "Comment",
    id: string,
    content: string,
    datetime_created: string,
    _ref_post:  {
      __typename: "Post",
      id: string,
      content: string,
      files: Array< string | null > | null,
    },
    _ref_user:  {
      __typename: "User",
      id: string,
      username: string,
      user_mode: Mode,
      fullname: string,
      avatar: string | null,
      online_status: OnlineStatus | null,
      friendly_search: string | null,
      is_found_in_search: boolean | null,
      likes: Array< string | null > | null,
    },
  } | null,
};

export type OnDeleteCommentSubscription = {
  onDeleteComment:  {
    __typename: "Comment",
    id: string,
    content: string,
    datetime_created: string,
    _ref_post:  {
      __typename: "Post",
      id: string,
      content: string,
      files: Array< string | null > | null,
    },
    _ref_user:  {
      __typename: "User",
      id: string,
      username: string,
      user_mode: Mode,
      fullname: string,
      avatar: string | null,
      online_status: OnlineStatus | null,
      friendly_search: string | null,
      is_found_in_search: boolean | null,
      likes: Array< string | null > | null,
    },
  } | null,
};

export type OnCreateLikeSubscription = {
  onCreateLike:  {
    __typename: "Like",
    postId_userId: string,
    post:  {
      __typename: "Post",
      id: string,
      content: string,
      files: Array< string | null > | null,
    },
    _ref_user:  {
      __typename: "User",
      id: string,
      username: string,
      user_mode: Mode,
      fullname: string,
      avatar: string | null,
      online_status: OnlineStatus | null,
      friendly_search: string | null,
      is_found_in_search: boolean | null,
      likes: Array< string | null > | null,
    },
  } | null,
};

export type OnUpdateLikeSubscription = {
  onUpdateLike:  {
    __typename: "Like",
    postId_userId: string,
    post:  {
      __typename: "Post",
      id: string,
      content: string,
      files: Array< string | null > | null,
    },
    _ref_user:  {
      __typename: "User",
      id: string,
      username: string,
      user_mode: Mode,
      fullname: string,
      avatar: string | null,
      online_status: OnlineStatus | null,
      friendly_search: string | null,
      is_found_in_search: boolean | null,
      likes: Array< string | null > | null,
    },
  } | null,
};

export type OnDeleteLikeSubscription = {
  onDeleteLike:  {
    __typename: "Like",
    postId_userId: string,
    post:  {
      __typename: "Post",
      id: string,
      content: string,
      files: Array< string | null > | null,
    },
    _ref_user:  {
      __typename: "User",
      id: string,
      username: string,
      user_mode: Mode,
      fullname: string,
      avatar: string | null,
      online_status: OnlineStatus | null,
      friendly_search: string | null,
      is_found_in_search: boolean | null,
      likes: Array< string | null > | null,
    },
  } | null,
};

export type OnCreateDocumentSubscription = {
  onCreateDocument:  {
    __typename: "Document",
    id: string,
    doc_title: string,
    deadline: string | null,
    doc_type: DocumentType,
    doc_questions:  Array< {
      __typename: "DocumentContent",
      question_id: string,
      question: string,
    } | null > | null,
    _ref_user_created:  {
      __typename: "User",
      id: string,
      username: string,
      user_mode: Mode,
      fullname: string,
      avatar: string | null,
      online_status: OnlineStatus | null,
      friendly_search: string | null,
      is_found_in_search: boolean | null,
      likes: Array< string | null > | null,
    },
    _ref_members_joined:  {
      __typename: "ModelMemberInDocumentConnection",
      nextToken: string | null,
    } | null,
  } | null,
};

export type OnUpdateDocumentSubscription = {
  onUpdateDocument:  {
    __typename: "Document",
    id: string,
    doc_title: string,
    deadline: string | null,
    doc_type: DocumentType,
    doc_questions:  Array< {
      __typename: "DocumentContent",
      question_id: string,
      question: string,
    } | null > | null,
    _ref_user_created:  {
      __typename: "User",
      id: string,
      username: string,
      user_mode: Mode,
      fullname: string,
      avatar: string | null,
      online_status: OnlineStatus | null,
      friendly_search: string | null,
      is_found_in_search: boolean | null,
      likes: Array< string | null > | null,
    },
    _ref_members_joined:  {
      __typename: "ModelMemberInDocumentConnection",
      nextToken: string | null,
    } | null,
  } | null,
};

export type OnDeleteDocumentSubscription = {
  onDeleteDocument:  {
    __typename: "Document",
    id: string,
    doc_title: string,
    deadline: string | null,
    doc_type: DocumentType,
    doc_questions:  Array< {
      __typename: "DocumentContent",
      question_id: string,
      question: string,
    } | null > | null,
    _ref_user_created:  {
      __typename: "User",
      id: string,
      username: string,
      user_mode: Mode,
      fullname: string,
      avatar: string | null,
      online_status: OnlineStatus | null,
      friendly_search: string | null,
      is_found_in_search: boolean | null,
      likes: Array< string | null > | null,
    },
    _ref_members_joined:  {
      __typename: "ModelMemberInDocumentConnection",
      nextToken: string | null,
    } | null,
  } | null,
};

export type OnCreateMemberInDocumentSubscription = {
  onCreateMemberInDocument:  {
    __typename: "MemberInDocument",
    id: string,
    answers:  Array< {
      __typename: "DocumentAnswerOfMember",
      question_id: string,
      answer_ids: Array< string > | null,
    } | null > | null,
    _ref_doc:  {
      __typename: "Document",
      id: string,
      doc_title: string,
      deadline: string | null,
      doc_type: DocumentType,
    },
    _ref_user:  {
      __typename: "User",
      id: string,
      username: string,
      user_mode: Mode,
      fullname: string,
      avatar: string | null,
      online_status: OnlineStatus | null,
      friendly_search: string | null,
      is_found_in_search: boolean | null,
      likes: Array< string | null > | null,
    },
  } | null,
};

export type OnUpdateMemberInDocumentSubscription = {
  onUpdateMemberInDocument:  {
    __typename: "MemberInDocument",
    id: string,
    answers:  Array< {
      __typename: "DocumentAnswerOfMember",
      question_id: string,
      answer_ids: Array< string > | null,
    } | null > | null,
    _ref_doc:  {
      __typename: "Document",
      id: string,
      doc_title: string,
      deadline: string | null,
      doc_type: DocumentType,
    },
    _ref_user:  {
      __typename: "User",
      id: string,
      username: string,
      user_mode: Mode,
      fullname: string,
      avatar: string | null,
      online_status: OnlineStatus | null,
      friendly_search: string | null,
      is_found_in_search: boolean | null,
      likes: Array< string | null > | null,
    },
  } | null,
};

export type OnDeleteMemberInDocumentSubscription = {
  onDeleteMemberInDocument:  {
    __typename: "MemberInDocument",
    id: string,
    answers:  Array< {
      __typename: "DocumentAnswerOfMember",
      question_id: string,
      answer_ids: Array< string > | null,
    } | null > | null,
    _ref_doc:  {
      __typename: "Document",
      id: string,
      doc_title: string,
      deadline: string | null,
      doc_type: DocumentType,
    },
    _ref_user:  {
      __typename: "User",
      id: string,
      username: string,
      user_mode: Mode,
      fullname: string,
      avatar: string | null,
      online_status: OnlineStatus | null,
      friendly_search: string | null,
      is_found_in_search: boolean | null,
      likes: Array< string | null > | null,
    },
  } | null,
};

export type OnCreateScheduleSubscription = {
  onCreateSchedule:  {
    __typename: "Schedule",
    datetime_created: string | null,
    datetime_expired: string | null,
    datetime_start: string | null,
    datetime_finish: string | null,
    day_of_week: string | null,
    day_of_year: string | null,
    month_of_year: string | null,
  } | null,
};

export type OnUpdateScheduleSubscription = {
  onUpdateSchedule:  {
    __typename: "Schedule",
    datetime_created: string | null,
    datetime_expired: string | null,
    datetime_start: string | null,
    datetime_finish: string | null,
    day_of_week: string | null,
    day_of_year: string | null,
    month_of_year: string | null,
  } | null,
};

export type OnDeleteScheduleSubscription = {
  onDeleteSchedule:  {
    __typename: "Schedule",
    datetime_created: string | null,
    datetime_expired: string | null,
    datetime_start: string | null,
    datetime_finish: string | null,
    day_of_week: string | null,
    day_of_year: string | null,
    month_of_year: string | null,
  } | null,
};
