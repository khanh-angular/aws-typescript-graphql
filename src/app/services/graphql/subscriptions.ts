// tslint:disable
// this is an auto generated file. This will be overwritten

export const onCreateUser = `subscription OnCreateUser {
  onCreateUser {
    id
    username
    user_mode
    fullname
    avatar
    online_status
    friendly_search
    is_found_in_search
    location {
      lat
      lng
    }
    likes
    pages_created {
      nextToken
    }
    pages_joined {
      nextToken
    }
    documents_created {
      nextToken
    }
    documents_joined {
      nextToken
    }
  }
}
`;
export const onUpdateUser = `subscription OnUpdateUser {
  onUpdateUser {
    id
    username
    user_mode
    fullname
    avatar
    online_status
    friendly_search
    is_found_in_search
    location {
      lat
      lng
    }
    likes
    pages_created {
      nextToken
    }
    pages_joined {
      nextToken
    }
    documents_created {
      nextToken
    }
    documents_joined {
      nextToken
    }
  }
}
`;
export const onDeleteUser = `subscription OnDeleteUser {
  onDeleteUser {
    id
    username
    user_mode
    fullname
    avatar
    online_status
    friendly_search
    is_found_in_search
    location {
      lat
      lng
    }
    likes
    pages_created {
      nextToken
    }
    pages_joined {
      nextToken
    }
    documents_created {
      nextToken
    }
    documents_joined {
      nextToken
    }
  }
}
`;
export const onCreatePage = `subscription OnCreatePage {
  onCreatePage {
    id
    page_name
    page_mode
    page_type
    current_members_count
    limit_members_count
    is_freedom
    is_found_in_search
    is_found_in_profile
    schedule {
      datetime_created
      datetime_expired
      datetime_start
      datetime_finish
      day_of_week
      day_of_year
      month_of_year
    }
    members {
      nextToken
    }
    posts {
      nextToken
    }
    _ref_user_created {
      id
      username
      user_mode
      fullname
      avatar
      online_status
      friendly_search
      is_found_in_search
      likes
    }
  }
}
`;
export const onUpdatePage = `subscription OnUpdatePage {
  onUpdatePage {
    id
    page_name
    page_mode
    page_type
    current_members_count
    limit_members_count
    is_freedom
    is_found_in_search
    is_found_in_profile
    schedule {
      datetime_created
      datetime_expired
      datetime_start
      datetime_finish
      day_of_week
      day_of_year
      month_of_year
    }
    members {
      nextToken
    }
    posts {
      nextToken
    }
    _ref_user_created {
      id
      username
      user_mode
      fullname
      avatar
      online_status
      friendly_search
      is_found_in_search
      likes
    }
  }
}
`;
export const onDeletePage = `subscription OnDeletePage {
  onDeletePage {
    id
    page_name
    page_mode
    page_type
    current_members_count
    limit_members_count
    is_freedom
    is_found_in_search
    is_found_in_profile
    schedule {
      datetime_created
      datetime_expired
      datetime_start
      datetime_finish
      day_of_week
      day_of_year
      month_of_year
    }
    members {
      nextToken
    }
    posts {
      nextToken
    }
    _ref_user_created {
      id
      username
      user_mode
      fullname
      avatar
      online_status
      friendly_search
      is_found_in_search
      likes
    }
  }
}
`;
export const onCreatePageMember = `subscription OnCreatePageMember {
  onCreatePageMember {
    pageId_userId
    score_count
    is_confirmed
    _page {
      id
      page_name
      page_mode
      page_type
      current_members_count
      limit_members_count
      is_freedom
      is_found_in_search
      is_found_in_profile
    }
    _user {
      id
      username
      user_mode
      fullname
      avatar
      online_status
      friendly_search
      is_found_in_search
      likes
    }
  }
}
`;
export const onUpdatePageMember = `subscription OnUpdatePageMember {
  onUpdatePageMember {
    pageId_userId
    score_count
    is_confirmed
    _page {
      id
      page_name
      page_mode
      page_type
      current_members_count
      limit_members_count
      is_freedom
      is_found_in_search
      is_found_in_profile
    }
    _user {
      id
      username
      user_mode
      fullname
      avatar
      online_status
      friendly_search
      is_found_in_search
      likes
    }
  }
}
`;
export const onDeletePageMember = `subscription OnDeletePageMember {
  onDeletePageMember {
    pageId_userId
    score_count
    is_confirmed
    _page {
      id
      page_name
      page_mode
      page_type
      current_members_count
      limit_members_count
      is_freedom
      is_found_in_search
      is_found_in_profile
    }
    _user {
      id
      username
      user_mode
      fullname
      avatar
      online_status
      friendly_search
      is_found_in_search
      likes
    }
  }
}
`;
export const onCreatePost = `subscription OnCreatePost {
  onCreatePost {
    id
    content
    files
    likes {
      nextToken
    }
    comments {
      nextToken
    }
    _page {
      id
      page_name
      page_mode
      page_type
      current_members_count
      limit_members_count
      is_freedom
      is_found_in_search
      is_found_in_profile
    }
    _ref_user_created {
      id
      username
      user_mode
      fullname
      avatar
      online_status
      friendly_search
      is_found_in_search
      likes
    }
  }
}
`;
export const onUpdatePost = `subscription OnUpdatePost {
  onUpdatePost {
    id
    content
    files
    likes {
      nextToken
    }
    comments {
      nextToken
    }
    _page {
      id
      page_name
      page_mode
      page_type
      current_members_count
      limit_members_count
      is_freedom
      is_found_in_search
      is_found_in_profile
    }
    _ref_user_created {
      id
      username
      user_mode
      fullname
      avatar
      online_status
      friendly_search
      is_found_in_search
      likes
    }
  }
}
`;
export const onDeletePost = `subscription OnDeletePost {
  onDeletePost {
    id
    content
    files
    likes {
      nextToken
    }
    comments {
      nextToken
    }
    _page {
      id
      page_name
      page_mode
      page_type
      current_members_count
      limit_members_count
      is_freedom
      is_found_in_search
      is_found_in_profile
    }
    _ref_user_created {
      id
      username
      user_mode
      fullname
      avatar
      online_status
      friendly_search
      is_found_in_search
      likes
    }
  }
}
`;
export const onCreateComment = `subscription OnCreateComment {
  onCreateComment {
    id
    content
    datetime_created
    _ref_post {
      id
      content
      files
    }
    _ref_user {
      id
      username
      user_mode
      fullname
      avatar
      online_status
      friendly_search
      is_found_in_search
      likes
    }
  }
}
`;
export const onUpdateComment = `subscription OnUpdateComment {
  onUpdateComment {
    id
    content
    datetime_created
    _ref_post {
      id
      content
      files
    }
    _ref_user {
      id
      username
      user_mode
      fullname
      avatar
      online_status
      friendly_search
      is_found_in_search
      likes
    }
  }
}
`;
export const onDeleteComment = `subscription OnDeleteComment {
  onDeleteComment {
    id
    content
    datetime_created
    _ref_post {
      id
      content
      files
    }
    _ref_user {
      id
      username
      user_mode
      fullname
      avatar
      online_status
      friendly_search
      is_found_in_search
      likes
    }
  }
}
`;
export const onCreateLike = `subscription OnCreateLike {
  onCreateLike {
    postId_userId
    post {
      id
      content
      files
    }
    _ref_user {
      id
      username
      user_mode
      fullname
      avatar
      online_status
      friendly_search
      is_found_in_search
      likes
    }
  }
}
`;
export const onUpdateLike = `subscription OnUpdateLike {
  onUpdateLike {
    postId_userId
    post {
      id
      content
      files
    }
    _ref_user {
      id
      username
      user_mode
      fullname
      avatar
      online_status
      friendly_search
      is_found_in_search
      likes
    }
  }
}
`;
export const onDeleteLike = `subscription OnDeleteLike {
  onDeleteLike {
    postId_userId
    post {
      id
      content
      files
    }
    _ref_user {
      id
      username
      user_mode
      fullname
      avatar
      online_status
      friendly_search
      is_found_in_search
      likes
    }
  }
}
`;
export const onCreateDocument = `subscription OnCreateDocument {
  onCreateDocument {
    id
    doc_title
    deadline
    doc_type
    doc_questions {
      question_id
      question
    }
    _ref_user_created {
      id
      username
      user_mode
      fullname
      avatar
      online_status
      friendly_search
      is_found_in_search
      likes
    }
    _ref_members_joined {
      nextToken
    }
  }
}
`;
export const onUpdateDocument = `subscription OnUpdateDocument {
  onUpdateDocument {
    id
    doc_title
    deadline
    doc_type
    doc_questions {
      question_id
      question
    }
    _ref_user_created {
      id
      username
      user_mode
      fullname
      avatar
      online_status
      friendly_search
      is_found_in_search
      likes
    }
    _ref_members_joined {
      nextToken
    }
  }
}
`;
export const onDeleteDocument = `subscription OnDeleteDocument {
  onDeleteDocument {
    id
    doc_title
    deadline
    doc_type
    doc_questions {
      question_id
      question
    }
    _ref_user_created {
      id
      username
      user_mode
      fullname
      avatar
      online_status
      friendly_search
      is_found_in_search
      likes
    }
    _ref_members_joined {
      nextToken
    }
  }
}
`;
export const onCreateMemberInDocument = `subscription OnCreateMemberInDocument {
  onCreateMemberInDocument {
    id
    answers {
      question_id
      answer_ids
    }
    _ref_doc {
      id
      doc_title
      deadline
      doc_type
    }
    _ref_user {
      id
      username
      user_mode
      fullname
      avatar
      online_status
      friendly_search
      is_found_in_search
      likes
    }
  }
}
`;
export const onUpdateMemberInDocument = `subscription OnUpdateMemberInDocument {
  onUpdateMemberInDocument {
    id
    answers {
      question_id
      answer_ids
    }
    _ref_doc {
      id
      doc_title
      deadline
      doc_type
    }
    _ref_user {
      id
      username
      user_mode
      fullname
      avatar
      online_status
      friendly_search
      is_found_in_search
      likes
    }
  }
}
`;
export const onDeleteMemberInDocument = `subscription OnDeleteMemberInDocument {
  onDeleteMemberInDocument {
    id
    answers {
      question_id
      answer_ids
    }
    _ref_doc {
      id
      doc_title
      deadline
      doc_type
    }
    _ref_user {
      id
      username
      user_mode
      fullname
      avatar
      online_status
      friendly_search
      is_found_in_search
      likes
    }
  }
}
`;
export const onCreateSchedule = `subscription OnCreateSchedule {
  onCreateSchedule {
    datetime_created
    datetime_expired
    datetime_start
    datetime_finish
    day_of_week
    day_of_year
    month_of_year
  }
}
`;
export const onUpdateSchedule = `subscription OnUpdateSchedule {
  onUpdateSchedule {
    datetime_created
    datetime_expired
    datetime_start
    datetime_finish
    day_of_week
    day_of_year
    month_of_year
  }
}
`;
export const onDeleteSchedule = `subscription OnDeleteSchedule {
  onDeleteSchedule {
    datetime_created
    datetime_expired
    datetime_start
    datetime_finish
    day_of_week
    day_of_year
    month_of_year
  }
}
`;
