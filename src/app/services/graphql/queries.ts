// tslint:disable
// this is an auto generated file. This will be overwritten

export const usersNearByMe = `query UsersNearByMe($location: LocationInput!, $km: Float!) {
  usersNearByMe(location: $location, km: $km) {
    id
    username
    user_mode
    fullname
    avatar
    online_status
    friendly_search
    is_found_in_search
    location {
      lat
      lng
    }
    likes
    pages_created {
      nextToken
    }
    pages_joined {
      nextToken
    }
    documents_created {
      nextToken
    }
    documents_joined {
      nextToken
    }
  }
}
`;
export const getUser = `query GetUser($id: ID!) {
  getUser(id: $id) {
    id
    username
    user_mode
    fullname
    avatar
    online_status
    friendly_search
    is_found_in_search
    location {
      lat
      lng
    }
    likes
    pages_created {
      nextToken
    }
    pages_joined {
      nextToken
    }
    documents_created {
      nextToken
    }
    documents_joined {
      nextToken
    }
  }
}
`;
export const listUsers = `query ListUsers(
  $filter: ModelUserFilterInput
  $limit: Int
  $nextToken: String
) {
  listUsers(filter: $filter, limit: $limit, nextToken: $nextToken) {
    items {
      id
      username
      user_mode
      fullname
      avatar
      online_status
      friendly_search
      is_found_in_search
      likes
    }
    nextToken
  }
}
`;
export const getPage = `query GetPage($id: ID!) {
  getPage(id: $id) {
    id
    page_name
    page_mode
    page_type
    current_members_count
    limit_members_count
    is_freedom
    is_found_in_search
    is_found_in_profile
    schedule {
      datetime_created
      datetime_expired
      datetime_start
      datetime_finish
      day_of_week
      day_of_year
      month_of_year
    }
    members {
      nextToken
    }
    posts {
      nextToken
    }
    _ref_user_created {
      id
      username
      user_mode
      fullname
      avatar
      online_status
      friendly_search
      is_found_in_search
      likes
    }
  }
}
`;
export const listPages = `query ListPages(
  $filter: ModelPageFilterInput
  $limit: Int
  $nextToken: String
) {
  listPages(filter: $filter, limit: $limit, nextToken: $nextToken) {
    items {
      id
      page_name
      page_mode
      page_type
      current_members_count
      limit_members_count
      is_freedom
      is_found_in_search
      is_found_in_profile
    }
    nextToken
  }
}
`;
export const getPageMember = `query GetPageMember($pageId_userId: ID!) {
  getPageMember(pageId_userId: $pageId_userId) {
    pageId_userId
    score_count
    is_confirmed
    _page {
      id
      page_name
      page_mode
      page_type
      current_members_count
      limit_members_count
      is_freedom
      is_found_in_search
      is_found_in_profile
    }
    _user {
      id
      username
      user_mode
      fullname
      avatar
      online_status
      friendly_search
      is_found_in_search
      likes
    }
  }
}
`;
export const listPageMembers = `query ListPageMembers(
  $pageId_userId: ID
  $filter: ModelPageMemberFilterInput
  $limit: Int
  $nextToken: String
  $sortDirection: ModelSortDirection
) {
  listPageMembers(
    pageId_userId: $pageId_userId
    filter: $filter
    limit: $limit
    nextToken: $nextToken
    sortDirection: $sortDirection
  ) {
    items {
      pageId_userId
      score_count
      is_confirmed
    }
    nextToken
  }
}
`;
export const getPost = `query GetPost($id: ID!) {
  getPost(id: $id) {
    id
    content
    files
    likes {
      nextToken
    }
    comments {
      nextToken
    }
    _page {
      id
      page_name
      page_mode
      page_type
      current_members_count
      limit_members_count
      is_freedom
      is_found_in_search
      is_found_in_profile
    }
    _ref_user_created {
      id
      username
      user_mode
      fullname
      avatar
      online_status
      friendly_search
      is_found_in_search
      likes
    }
  }
}
`;
export const listPosts = `query ListPosts(
  $filter: ModelPostFilterInput
  $limit: Int
  $nextToken: String
) {
  listPosts(filter: $filter, limit: $limit, nextToken: $nextToken) {
    items {
      id
      content
      files
    }
    nextToken
  }
}
`;
export const getComment = `query GetComment($id: ID!) {
  getComment(id: $id) {
    id
    content
    datetime_created
    _ref_post {
      id
      content
      files
    }
    _ref_user {
      id
      username
      user_mode
      fullname
      avatar
      online_status
      friendly_search
      is_found_in_search
      likes
    }
  }
}
`;
export const listComments = `query ListComments(
  $filter: ModelCommentFilterInput
  $limit: Int
  $nextToken: String
) {
  listComments(filter: $filter, limit: $limit, nextToken: $nextToken) {
    items {
      id
      content
      datetime_created
    }
    nextToken
  }
}
`;
export const getLike = `query GetLike($postId_userId: ID!) {
  getLike(postId_userId: $postId_userId) {
    postId_userId
    post {
      id
      content
      files
    }
    _ref_user {
      id
      username
      user_mode
      fullname
      avatar
      online_status
      friendly_search
      is_found_in_search
      likes
    }
  }
}
`;
export const listLikes = `query ListLikes(
  $postId_userId: ID
  $filter: ModelLikeFilterInput
  $limit: Int
  $nextToken: String
  $sortDirection: ModelSortDirection
) {
  listLikes(
    postId_userId: $postId_userId
    filter: $filter
    limit: $limit
    nextToken: $nextToken
    sortDirection: $sortDirection
  ) {
    items {
      postId_userId
    }
    nextToken
  }
}
`;
export const getDocument = `query GetDocument($id: ID!) {
  getDocument(id: $id) {
    id
    doc_title
    deadline
    doc_type
    doc_questions {
      question_id
      question
    }
    _ref_user_created {
      id
      username
      user_mode
      fullname
      avatar
      online_status
      friendly_search
      is_found_in_search
      likes
    }
    _ref_members_joined {
      nextToken
    }
  }
}
`;
export const listDocuments = `query ListDocuments(
  $filter: ModelDocumentFilterInput
  $limit: Int
  $nextToken: String
) {
  listDocuments(filter: $filter, limit: $limit, nextToken: $nextToken) {
    items {
      id
      doc_title
      deadline
      doc_type
    }
    nextToken
  }
}
`;
export const getMemberInDocument = `query GetMemberInDocument($id: ID!) {
  getMemberInDocument(id: $id) {
    id
    answers {
      question_id
      answer_ids
    }
    _ref_doc {
      id
      doc_title
      deadline
      doc_type
    }
    _ref_user {
      id
      username
      user_mode
      fullname
      avatar
      online_status
      friendly_search
      is_found_in_search
      likes
    }
  }
}
`;
export const listMemberInDocuments = `query ListMemberInDocuments(
  $filter: ModelMemberInDocumentFilterInput
  $limit: Int
  $nextToken: String
) {
  listMemberInDocuments(filter: $filter, limit: $limit, nextToken: $nextToken) {
    items {
      id
    }
    nextToken
  }
}
`;
export const getSchedule = `query GetSchedule($id: ID!) {
  getSchedule(id: $id) {
    datetime_created
    datetime_expired
    datetime_start
    datetime_finish
    day_of_week
    day_of_year
    month_of_year
  }
}
`;
export const listSchedules = `query ListSchedules(
  $filter: ModelScheduleFilterInput
  $limit: Int
  $nextToken: String
) {
  listSchedules(filter: $filter, limit: $limit, nextToken: $nextToken) {
    items {
      datetime_created
      datetime_expired
      datetime_start
      datetime_finish
      day_of_week
      day_of_year
      month_of_year
    }
    nextToken
  }
}
`;
export const listPublicPages = `query ListPublicPages(
  $page_type: PageTypeCategory
  $page_mode: ModelStringKeyConditionInput
  $sortDirection: ModelSortDirection
  $filter: ModelPageFilterInput
  $limit: Int
  $nextToken: String
) {
  listPublicPages(
    page_type: $page_type
    page_mode: $page_mode
    sortDirection: $sortDirection
    filter: $filter
    limit: $limit
    nextToken: $nextToken
  ) {
    items {
      id
      page_name
      page_mode
      page_type
      current_members_count
      limit_members_count
      is_freedom
      is_found_in_search
      is_found_in_profile
    }
    nextToken
  }
}
`;
export const searchUsers = `query SearchUsers(
  $filter: SearchableUserFilterInput
  $sort: SearchableUserSortInput
  $limit: Int
  $nextToken: String
) {
  searchUsers(
    filter: $filter
    sort: $sort
    limit: $limit
    nextToken: $nextToken
  ) {
    items {
      id
      username
      user_mode
      fullname
      avatar
      online_status
      friendly_search
      is_found_in_search
      likes
    }
    nextToken
  }
}
`;
export const searchPages = `query SearchPages(
  $filter: SearchablePageFilterInput
  $sort: SearchablePageSortInput
  $limit: Int
  $nextToken: String
) {
  searchPages(
    filter: $filter
    sort: $sort
    limit: $limit
    nextToken: $nextToken
  ) {
    items {
      id
      page_name
      page_mode
      page_type
      current_members_count
      limit_members_count
      is_freedom
      is_found_in_search
      is_found_in_profile
    }
    nextToken
  }
}
`;
