// tslint:disable
// this is an auto generated file. This will be overwritten

export const createUser = `mutation CreateUser($input: CreateUserInput!) {
  createUser(input: $input) {
    id
    username
    user_mode
    fullname
    avatar
    online_status
    friendly_search
    is_found_in_search
    location {
      lat
      lng
    }
    likes
    pages_created {
      nextToken
    }
    pages_joined {
      nextToken
    }
    documents_created {
      nextToken
    }
    documents_joined {
      nextToken
    }
  }
}
`;
export const updateUser = `mutation UpdateUser($input: UpdateUserInput!) {
  updateUser(input: $input) {
    id
    username
    user_mode
    fullname
    avatar
    online_status
    friendly_search
    is_found_in_search
    location {
      lat
      lng
    }
    likes
    pages_created {
      nextToken
    }
    pages_joined {
      nextToken
    }
    documents_created {
      nextToken
    }
    documents_joined {
      nextToken
    }
  }
}
`;
export const deleteUser = `mutation DeleteUser($input: DeleteUserInput!) {
  deleteUser(input: $input) {
    id
    username
    user_mode
    fullname
    avatar
    online_status
    friendly_search
    is_found_in_search
    location {
      lat
      lng
    }
    likes
    pages_created {
      nextToken
    }
    pages_joined {
      nextToken
    }
    documents_created {
      nextToken
    }
    documents_joined {
      nextToken
    }
  }
}
`;
export const createPage = `mutation CreatePage($input: CreatePageInput!) {
  createPage(input: $input) {
    id
    page_name
    page_mode
    page_type
    current_members_count
    limit_members_count
    is_freedom
    is_found_in_search
    is_found_in_profile
    schedule {
      datetime_created
      datetime_expired
      datetime_start
      datetime_finish
      day_of_week
      day_of_year
      month_of_year
    }
    members {
      nextToken
    }
    posts {
      nextToken
    }
    _ref_user_created {
      id
      username
      user_mode
      fullname
      avatar
      online_status
      friendly_search
      is_found_in_search
      likes
    }
  }
}
`;
export const updatePage = `mutation UpdatePage($input: UpdatePageInput!) {
  updatePage(input: $input) {
    id
    page_name
    page_mode
    page_type
    current_members_count
    limit_members_count
    is_freedom
    is_found_in_search
    is_found_in_profile
    schedule {
      datetime_created
      datetime_expired
      datetime_start
      datetime_finish
      day_of_week
      day_of_year
      month_of_year
    }
    members {
      nextToken
    }
    posts {
      nextToken
    }
    _ref_user_created {
      id
      username
      user_mode
      fullname
      avatar
      online_status
      friendly_search
      is_found_in_search
      likes
    }
  }
}
`;
export const deletePage = `mutation DeletePage($input: DeletePageInput!) {
  deletePage(input: $input) {
    id
    page_name
    page_mode
    page_type
    current_members_count
    limit_members_count
    is_freedom
    is_found_in_search
    is_found_in_profile
    schedule {
      datetime_created
      datetime_expired
      datetime_start
      datetime_finish
      day_of_week
      day_of_year
      month_of_year
    }
    members {
      nextToken
    }
    posts {
      nextToken
    }
    _ref_user_created {
      id
      username
      user_mode
      fullname
      avatar
      online_status
      friendly_search
      is_found_in_search
      likes
    }
  }
}
`;
export const createPageMember = `mutation CreatePageMember($input: CreatePageMemberInput!) {
  createPageMember(input: $input) {
    pageId_userId
    score_count
    is_confirmed
    _page {
      id
      page_name
      page_mode
      page_type
      current_members_count
      limit_members_count
      is_freedom
      is_found_in_search
      is_found_in_profile
    }
    _user {
      id
      username
      user_mode
      fullname
      avatar
      online_status
      friendly_search
      is_found_in_search
      likes
    }
  }
}
`;
export const updatePageMember = `mutation UpdatePageMember($input: UpdatePageMemberInput!) {
  updatePageMember(input: $input) {
    pageId_userId
    score_count
    is_confirmed
    _page {
      id
      page_name
      page_mode
      page_type
      current_members_count
      limit_members_count
      is_freedom
      is_found_in_search
      is_found_in_profile
    }
    _user {
      id
      username
      user_mode
      fullname
      avatar
      online_status
      friendly_search
      is_found_in_search
      likes
    }
  }
}
`;
export const deletePageMember = `mutation DeletePageMember($input: DeletePageMemberInput!) {
  deletePageMember(input: $input) {
    pageId_userId
    score_count
    is_confirmed
    _page {
      id
      page_name
      page_mode
      page_type
      current_members_count
      limit_members_count
      is_freedom
      is_found_in_search
      is_found_in_profile
    }
    _user {
      id
      username
      user_mode
      fullname
      avatar
      online_status
      friendly_search
      is_found_in_search
      likes
    }
  }
}
`;
export const createPost = `mutation CreatePost($input: CreatePostInput!) {
  createPost(input: $input) {
    id
    content
    files
    likes {
      nextToken
    }
    comments {
      nextToken
    }
    _page {
      id
      page_name
      page_mode
      page_type
      current_members_count
      limit_members_count
      is_freedom
      is_found_in_search
      is_found_in_profile
    }
    _ref_user_created {
      id
      username
      user_mode
      fullname
      avatar
      online_status
      friendly_search
      is_found_in_search
      likes
    }
  }
}
`;
export const updatePost = `mutation UpdatePost($input: UpdatePostInput!) {
  updatePost(input: $input) {
    id
    content
    files
    likes {
      nextToken
    }
    comments {
      nextToken
    }
    _page {
      id
      page_name
      page_mode
      page_type
      current_members_count
      limit_members_count
      is_freedom
      is_found_in_search
      is_found_in_profile
    }
    _ref_user_created {
      id
      username
      user_mode
      fullname
      avatar
      online_status
      friendly_search
      is_found_in_search
      likes
    }
  }
}
`;
export const deletePost = `mutation DeletePost($input: DeletePostInput!) {
  deletePost(input: $input) {
    id
    content
    files
    likes {
      nextToken
    }
    comments {
      nextToken
    }
    _page {
      id
      page_name
      page_mode
      page_type
      current_members_count
      limit_members_count
      is_freedom
      is_found_in_search
      is_found_in_profile
    }
    _ref_user_created {
      id
      username
      user_mode
      fullname
      avatar
      online_status
      friendly_search
      is_found_in_search
      likes
    }
  }
}
`;
export const createComment = `mutation CreateComment($input: CreateCommentInput!) {
  createComment(input: $input) {
    id
    content
    datetime_created
    _ref_post {
      id
      content
      files
    }
    _ref_user {
      id
      username
      user_mode
      fullname
      avatar
      online_status
      friendly_search
      is_found_in_search
      likes
    }
  }
}
`;
export const updateComment = `mutation UpdateComment($input: UpdateCommentInput!) {
  updateComment(input: $input) {
    id
    content
    datetime_created
    _ref_post {
      id
      content
      files
    }
    _ref_user {
      id
      username
      user_mode
      fullname
      avatar
      online_status
      friendly_search
      is_found_in_search
      likes
    }
  }
}
`;
export const deleteComment = `mutation DeleteComment($input: DeleteCommentInput!) {
  deleteComment(input: $input) {
    id
    content
    datetime_created
    _ref_post {
      id
      content
      files
    }
    _ref_user {
      id
      username
      user_mode
      fullname
      avatar
      online_status
      friendly_search
      is_found_in_search
      likes
    }
  }
}
`;
export const createLike = `mutation CreateLike($input: CreateLikeInput!) {
  createLike(input: $input) {
    postId_userId
    post {
      id
      content
      files
    }
    _ref_user {
      id
      username
      user_mode
      fullname
      avatar
      online_status
      friendly_search
      is_found_in_search
      likes
    }
  }
}
`;
export const updateLike = `mutation UpdateLike($input: UpdateLikeInput!) {
  updateLike(input: $input) {
    postId_userId
    post {
      id
      content
      files
    }
    _ref_user {
      id
      username
      user_mode
      fullname
      avatar
      online_status
      friendly_search
      is_found_in_search
      likes
    }
  }
}
`;
export const deleteLike = `mutation DeleteLike($input: DeleteLikeInput!) {
  deleteLike(input: $input) {
    postId_userId
    post {
      id
      content
      files
    }
    _ref_user {
      id
      username
      user_mode
      fullname
      avatar
      online_status
      friendly_search
      is_found_in_search
      likes
    }
  }
}
`;
export const createDocument = `mutation CreateDocument($input: CreateDocumentInput!) {
  createDocument(input: $input) {
    id
    doc_title
    deadline
    doc_type
    doc_questions {
      question_id
      question
    }
    _ref_user_created {
      id
      username
      user_mode
      fullname
      avatar
      online_status
      friendly_search
      is_found_in_search
      likes
    }
    _ref_members_joined {
      nextToken
    }
  }
}
`;
export const updateDocument = `mutation UpdateDocument($input: UpdateDocumentInput!) {
  updateDocument(input: $input) {
    id
    doc_title
    deadline
    doc_type
    doc_questions {
      question_id
      question
    }
    _ref_user_created {
      id
      username
      user_mode
      fullname
      avatar
      online_status
      friendly_search
      is_found_in_search
      likes
    }
    _ref_members_joined {
      nextToken
    }
  }
}
`;
export const deleteDocument = `mutation DeleteDocument($input: DeleteDocumentInput!) {
  deleteDocument(input: $input) {
    id
    doc_title
    deadline
    doc_type
    doc_questions {
      question_id
      question
    }
    _ref_user_created {
      id
      username
      user_mode
      fullname
      avatar
      online_status
      friendly_search
      is_found_in_search
      likes
    }
    _ref_members_joined {
      nextToken
    }
  }
}
`;
export const createMemberInDocument = `mutation CreateMemberInDocument($input: CreateMemberInDocumentInput!) {
  createMemberInDocument(input: $input) {
    id
    answers {
      question_id
      answer_ids
    }
    _ref_doc {
      id
      doc_title
      deadline
      doc_type
    }
    _ref_user {
      id
      username
      user_mode
      fullname
      avatar
      online_status
      friendly_search
      is_found_in_search
      likes
    }
  }
}
`;
export const updateMemberInDocument = `mutation UpdateMemberInDocument($input: UpdateMemberInDocumentInput!) {
  updateMemberInDocument(input: $input) {
    id
    answers {
      question_id
      answer_ids
    }
    _ref_doc {
      id
      doc_title
      deadline
      doc_type
    }
    _ref_user {
      id
      username
      user_mode
      fullname
      avatar
      online_status
      friendly_search
      is_found_in_search
      likes
    }
  }
}
`;
export const deleteMemberInDocument = `mutation DeleteMemberInDocument($input: DeleteMemberInDocumentInput!) {
  deleteMemberInDocument(input: $input) {
    id
    answers {
      question_id
      answer_ids
    }
    _ref_doc {
      id
      doc_title
      deadline
      doc_type
    }
    _ref_user {
      id
      username
      user_mode
      fullname
      avatar
      online_status
      friendly_search
      is_found_in_search
      likes
    }
  }
}
`;
export const createSchedule = `mutation CreateSchedule($input: CreateScheduleInput!) {
  createSchedule(input: $input) {
    datetime_created
    datetime_expired
    datetime_start
    datetime_finish
    day_of_week
    day_of_year
    month_of_year
  }
}
`;
export const updateSchedule = `mutation UpdateSchedule($input: UpdateScheduleInput!) {
  updateSchedule(input: $input) {
    datetime_created
    datetime_expired
    datetime_start
    datetime_finish
    day_of_week
    day_of_year
    month_of_year
  }
}
`;
export const deleteSchedule = `mutation DeleteSchedule($input: DeleteScheduleInput!) {
  deleteSchedule(input: $input) {
    datetime_created
    datetime_expired
    datetime_start
    datetime_finish
    day_of_week
    day_of_year
    month_of_year
  }
}
`;
