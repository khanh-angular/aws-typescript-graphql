import { NgModule } from '@angular/core';
import { LangPipe } from '@cedu/pipes/lang.pipe';

@NgModule({
    declarations: [
        LangPipe
    ],
    imports: [],
    providers: [],
    exports: [
        LangPipe
    ],
    bootstrap: [],
})
export class AppPipesShared {}
