import { Component } from '@angular/core';
import { AuthService } from 'src/app/services/auth.service';
import { StorageHelper } from '@cedu-helpers/storage.helper';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.scss']
})
export class NavbarComponent {
  storageHelper = new StorageHelper();
  constructor(public auth: AuthService) {}

  setLang(lang: string) {
    this.storageHelper.setItem('Lang', lang);
    window.location.reload();
  }
}
