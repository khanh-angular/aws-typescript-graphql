import { Component, OnInit, ViewChild, ElementRef, AfterViewInit } from '@angular/core';
import { Store, select } from '@ngrx/store';
import { selectAllUsers } from '@cedu-store/user/user.selectors';
import { loadUsers } from '@cedu-store/user/user.actions';
import { Observable } from 'rxjs';
import { User } from '@cedu-models/user.model';
import { API, graphqlOperation  } from 'aws-amplify';
// import { CacheObject } from '@aws-amplify/cache/src/Utils';
@Component({
  selector: 'app-space',
  templateUrl: './space.component.html',
  styleUrls: ['./space.component.scss'],
})
export class SpaceComponent implements OnInit, AfterViewInit {
  users: Observable<User[]>;
  @ViewChild('gcse', { static: true }) gdom: ElementRef;
  constructor(private store: Store<User[]>) {
    this.users = store.pipe(select(selectAllUsers));
  }

  ngOnInit() {
    // AWS.util.date.getDate().getTime();
  }

  getUsers() {
    this.store.dispatch(loadUsers());
  }

  createTask() {
    // API.graphql(graphqlOperation(createTask, {
    //   input: {
    //     title: 'asdx1',
    //     description: 'b',
    //     status: 'online'
    //   }
    // }));
  }

  ngAfterViewInit() {
  }
}
